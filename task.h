/* 
 * File:   task.h
 * Author: huyru
 *
 * Created on December 25, 2019, 4:33 PM
 */

#ifndef TASK_H
#define	TASK_H

#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif

    typedef enum {
        NORMAL_MODE,
        LEARN_MODE,
        //Add more states here
    } main_state_t;

    typedef enum {
        LED_HIGH,
        LED_LOW,
        //Add more states here
    } led_state_t;

    typedef enum {
        SOUND,
        SILENT,
        //Add more states here
    } buzzer_state_t;

    typedef enum {
        UP,
        DOWN,
        PAUSE,
        LOCKED,
        UNLOCKED,
    } control_state_t;

    uint8_t stuck_flag = 0;    
    
    void RF_Task(void);
    void Keeloq_Task(void);
    void Control_Task(void);
    void LED_Task(void);
    void Buzzer_Task(void);
    void Set_LED_Blink(uint16_t timeLength, uint8_t repeatNumber);
    void Set_Buzzer_Beep(uint16_t timeLength, uint8_t repeatNumber);

#ifdef	__cplusplus
}
#endif

#endif	/* TASK_H */

