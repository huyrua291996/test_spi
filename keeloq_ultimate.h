/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */

#ifndef KEELOQ_ULTIMATE_H
#define	KEELOQ_ULTIMATE_H

#include <stdint.h>
#include "encoder_types.h"
//#include "type.h"

//#define ULTIMATE_RESYNCH_RECEICER_ON_TRANSMITTER
#define ULTIMATE_RESYNCH_RECEICER_ON_RTCC

#define COUNTER_MAX_ADVANCE_ALLOWED 0x20

// Uncomment this line if counter checking is required
#define CHECK_ULTIMATE_COUNTER


typedef enum {
    ULTIMATE_NORMAL_MODE,
    ULTIMATE_LEARN,
    ULTIMATE_SECURE_LEARN,
    ULTIMATE_RESYNC
    //Add more States here
} ultimate_state_t;

typedef enum {
    ULTIMATE_SECURE_LEARN_SEED,
    ULTIMATE_SECURE_LEARN_NORMAL,
    //Add more States here
} ultimate_secure_learn_state_t;

uint8_t Decode_Ultimate_KeeLoq(rawdata_packet_t* indata);
void keeloq_ultimate_processEvents(event_t event);
void keeloq_ultimate_decrypt_packet(ultimate_encoder_t* data, rawdata_packet_t* inputData);
void keeloq_ultimate_init(void);
void ultimate_resync_receiver_timer(void);
void ultimate_erase_all(void);
//void transfer_to_record(ultimate_nvm_t ultimate_nvm);
//void extract_decrypted_data(rawdata_packet_t tmpPacket);
//void transfer_from_record(ultimate_nvm_t ultimate_nvm);
//void* keeloq_ultimate_processP2P(UART_P2P_COMMANDS cmd);


#endif	/* KEELOQ_ULTIMATE_H */

