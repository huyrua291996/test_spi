/**
  EEPROM I2C Source File

  Company:
    Microchip Technology Inc.

  File Name:
    eeprom_i2c.c

  Summary:
   This is the source file containing the EEPROM I2C functions and constants.

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB� Code Configurator - v2.25.2
        Device            :  PIC16F1719
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.34
        MPLAB             :  MPLAB X v2.35 or v3.00
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */

//#include "mcc_generated_files/mcc.h"
#include "i2c_rtc.h"
#include <xc.h>
#include "i2c.h"

extern uint8_t datab;

uint8_t timeOut = 0;


void I2C_ByteWrite1(uint8_t dataAddress, uint8_t dataByte) {
    uint8_t address[1];
    address[0] = dataAddress;
    I2C_ByteWrite(address, dataByte, 1);
}

uint8_t I2C_ByteRead1(uint8_t dataAddress) {
    uint8_t address[1], dataByte[1];
    address[0] = dataAddress;
    I2C_ByteRead(address, dataByte[0], 1);

    return datab;
}

int I2C_ByteWrite(uint8_t *dataAddress, uint8_t dataByte, uint8_t addlen) {
    uint8_t writeBuffer[PAGE_LIMIT + 3];
    uint8_t buflen;

    //Copy address bytes to the write buffer so it can be sent first 
    for (uint8_t i = 0; i < addlen; i++) {
        writeBuffer[i] = dataAddress[i];
    }

    //Check if this is an address write or a data write.
    if (dataByte != NULL) {
        writeBuffer[addlen] = dataByte;
        buflen = (uint8_t) (addlen + 1);
    }
    else
        buflen = addlen;

    //set status to Message Pending to send the data
    I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;

    //This variable is the built in acknowledge polling mechanism. This counts how many retries the system has already done to send the data.
    timeOut = 0;

    //While the message has not failed...
    while (status != I2C1_MESSAGE_FAIL) {
        // Initiate a write to EEPROM
        I2C1_MasterWrite(writeBuffer, buflen, 0x6F, &status);

        // wait for the message to be sent or status has changed.
        while (status == I2C1_MESSAGE_PENDING);
        // if transfer is complete, break the loop
        if (status == I2C1_MESSAGE_COMPLETE)
            break;
        // if transfer fails, break the loop
        if (status == I2C1_MESSAGE_FAIL)
            break;
        //Max retry is set for max Ack polling. If the Acknowledge bit is not set, this will just loop again until the write command is acknowledged
        if (timeOut == MAX_RETRY)
            break;
        else
            timeOut++;
    }
    // if the transfer failed, stop at this point
    if (status == I2C1_MESSAGE_FAIL)
        return 1;
    return 0;
}

uint8_t I2C_ByteRead(uint8_t *dataAddress, uint8_t dataByte, uint8_t addlen) {
    int check;

    //Write the address to the slave
    check = I2C_ByteWrite(dataAddress, NULL, addlen);

    //If not successful, return to function
    if (check == 1)
        return 1;

    //Get ready to send data
    I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
    //Set up for ACK polling
    timeOut = 0;

    //While the code has not detected message failure..
    while (status != I2C1_MESSAGE_FAIL) {
        // Initiate a Read to EEPROM 
        I2C1_MasterRead(&dataByte, 1, 0x6F, &status);

        // wait for the message to be sent or status has changed.
        while (status == I2C1_MESSAGE_PENDING);

        // if transfer is complete, break the loop
        if (status == I2C1_MESSAGE_COMPLETE)
            break;

        // if transfer fails, break the loop
        if (status == I2C1_MESSAGE_FAIL)
            break;

        // check for max retry and skip this byte
        if (timeOut == MAX_RETRY)
            break;
        else
            timeOut++;
    }
    return 0;
}

int I2C_BufferWrite(uint8_t *dataAddress, uint8_t *dataBuffer, uint8_t addlen, uint8_t buflen) {
    uint8_t writeBuffer[PAGE_LIMIT + 3];
    I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;

    //Set Address as the bytes to be written first
    for (uint8_t i = 0; i < addlen; i++) {
        writeBuffer[i] = dataAddress[i];
    }

    //Ensure that the page limit is not breached so as to avoid overwriting other data
    if (buflen > PAGE_LIMIT)
        buflen = PAGE_LIMIT;

    //Copy data bytes to write buffer
    for (uint8_t j = 0; j < buflen; j++) {
        writeBuffer[(uint8_t) (addlen + j)] = dataBuffer[j];
    }
    //Set up for ACK polling
    timeOut = 0;

    while (status != I2C1_MESSAGE_FAIL) {
        // Initiate a write to EEPROM
        I2C1_MasterWrite(writeBuffer, (uint8_t) (buflen + addlen), 0x6F, &status);

        // wait for the message to be sent or status has changed.
        while (status == I2C1_MESSAGE_PENDING);
        // if transfer is complete, break the loop
        if (status == I2C1_MESSAGE_COMPLETE)
            break;
        // if transfer fails, break the loop
        if (status == I2C1_MESSAGE_FAIL)
            break;

        //check for max retry and skip this byte
        if (timeOut == MAX_RETRY)
            break;
        else
            timeOut++;
    }
    // if the transfer failed, stop at this point
    if (status == I2C1_MESSAGE_FAIL)
        return 1;
    return 0;
}

void I2C_BufferRead(uint8_t *dataAddress, uint8_t *dataBuffer, uint8_t addlen, uint8_t buflen) {
    int check = 0;
    I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;

    //Write Address from where to read
    check = I2C_ByteWrite(dataAddress, NULL, addlen);

    //check if address write is successful
    if (check == 1)
        return;

    //Set up for ACK polling
    timeOut = 0;

    while (status != I2C1_MESSAGE_FAIL) {
        // Initiate a Read to EEPROM 
        I2C1_MasterRead(dataBuffer, buflen, 0x6F, &status);

        // wait for the message to be sent or status has changed.
        while (status == I2C1_MESSAGE_PENDING);

        // if transfer is complete, break the loop
        if (status == I2C1_MESSAGE_COMPLETE)
            break;

        // if transfer fails, break the loop
        if (status == I2C1_MESSAGE_FAIL)
            break;

        // check for max retry and skip this byte
        if (timeOut == MAX_RETRY)
            break;
        else
            timeOut++;
    }

}

uint8_t Get_Year(void) {
    uint8_t year;
    year = I2C_ByteRead1(0x06);
    year = ((year >> 4) & 0x0F)*10 + (year & 0x0F);
    return year;
}

uint8_t Get_Month(void) {
    uint8_t month;
    month = I2C_ByteRead1(0x05);
    month = ((month >> 4) & 0x01)*10 + (month & 0x0F);
    return month;
}

uint8_t Get_Day(void) {
    uint8_t day;
    day = I2C_ByteRead1(0x04);
    day = ((day >> 4) & 0x03)*10 + (day & 0x0F);
    return day;
}

uint8_t Get_Hour(void) {
    uint8_t hour;
    hour = I2C_ByteRead1(0x02);
    hour = ((hour >> 4)&0x03)*10 + (hour & 0x0F);
    return hour;
}

uint8_t Get_Minute(void) {
    uint8_t minute;
    minute = I2C_ByteRead1(0x01);
    minute = ((minute >> 4)&0x07)*10 + (minute & 0x0F);
    return minute;
}

uint8_t Get_Second(void) {
    uint8_t second;
    second = I2C_ByteRead1(0x00);
    second = ((second >> 4)&0x07)*10 + (second & 0x0F);
    return second;
}


void Set_Time(uint8_t year, uint8_t month, uint8_t day,
        uint8_t hour, uint8_t minute, uint8_t second) {
    //I2C_ByteWrite1(0x07, 0x00);
    uint8_t leap, wk;
    leap = ((year % 4) == 0) ? 1 : 0;
    year = (year % 10) + ((year / 10) << 4);
    I2C_ByteWrite1(0x06, year);
    month = (month % 10) + ((month / 10) << 4);
    if (leap == 1) (month |= (1 << 5));
    I2C_ByteWrite1(0x05, month);
    day = (day % 10) + ((day / 10) << 4);
    I2C_ByteWrite1(0x04, day);
    hour = (hour % 10) + ((hour / 10) << 4);
    I2C_ByteWrite1(0x02, hour);
    minute = (minute % 10) + ((minute / 10) << 4);
    I2C_ByteWrite1(0x01, minute);
    second = (second % 10) + ((second / 10) << 4);
    wk = I2C_ByteRead1(0x03);
    I2C_ByteWrite1(0x00, second);
    //I2C_ByteWrite1(0x07, 0x08);
    I2C_ByteWrite1(0x00, second | 0x80);
    I2C_ByteWrite1(0x03, wk | 0x20);
}

void MCP_Start(void) {
    uint8_t res;
    //I2C_ByteWrite1(0x03, 0x20);
    I2C_ByteWrite1(0x07, 0x00);
    res = I2C_ByteRead1(0x00);
    I2C_ByteWrite1(0x00, res | 0x80);
    res = I2C_ByteRead1(0x03);
    while (res == 0) {
        res = I2C_ByteRead1(0x03);
    }
    //I2C_ByteWrite1(0x03, res | 0x20);    
    //res = I2C_ByteRead1(0x00);
    //res = I2C_ByteRead1(0x03);
}