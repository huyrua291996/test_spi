/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */

#ifdef __XC8
#include <xc.h>
#else
#include <pic.h>
#endif
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "rxa_raw.h"
//#include "pictail.h"
#include "hardware.h"
#include "uart.h"

/** Longest high Te */
#define HIGH_TO     	7

/** Longest low Te */
#define LOW_TO       	7

/** Shortest Thead accepted */
#define SHORT_HEAD   	25

/** Longest Thead accepted */
#define LONG_HEAD    	45

/** Ideal Midpoint for 200us bit time transmissions */
#define MIDPOINT    ((3*200)/(2*60))-1 //use for last bit

typedef enum  {
    TRFreset,
    TRFSYNC,
    TRFUNO,
    TRFZERO,
    TRFSYNC0,
    TRFDATAREADY
} rxi_states_t;

static rxi_states_t RFstate = TRFreset;

static uint8_t RX_Buffer[SX1239RAW_MAXBUFFER];

static uint8_t BitCount; // Received bits counter
extern uint16_t packet;

uint8_t rxi_read(uint8_t * b, size_t s) {
    if (RFstate == TRFDATAREADY) {
        memcpy(b, RX_Buffer, s);
        memset(RX_Buffer, 0, sizeof(RX_Buffer));
        RFstate = TRFreset;
        //LED_TOG;
        //packet++;
        //LED2 ^= 1;

        return BitCount;
    } else {
        return 0;
    }
}

void rxi_tasks(void) {
    // this routine gets called every time TMR0 overflows
    static uint8_t High_Time = 0;
    static int8_t RFcount;
    static uint8_t Bptr; // Receive buffer pointer
    static uint8_t RFsynch; // Incoming trasnmission Synch level detect flag

    //static uint8_t RFBit; //each bit received is accessed in real time

    switch (RFstate) // state machine main switch
    {
        case TRFDATAREADY:
            // Data buffer still full.
            break;

        case TRFUNO: // Have detected a high level waiting for falling edge

            if (PICtail_RAW_DATA == 0) // Read RF level
            {   // falling edge detected  ----+
                //                            |
                //   	                      +-------

                RFstate = TRFZERO; // Falling edge detected, go back to state TRFZERO
            } else { // still high
                High_Time++;
                RFcount--;
                if (RFcount >= HIGH_TO) // Check for timeouot
                { // If timeout, then check for what type of transmission detected.
                    if (BitCount < SX1239RAW_MINBIT) // Not a complete transmission so reset state machine
                    {
                        //UART_SendString("High Reset!");
                        RFstate = TRFSYNC;
                        Bptr = 0;
                        BitCount = 0;
                        break;
                    }

                    RFstate = TRFDATAREADY;

                }//if RFcount
            }//else
            break;

        case TRFZERO: // We've detected a low level, look for rising edge
            if (PICtail_RAW_DATA) // Read RF bit.
            {   // rising edge detected     +----
                //                          |
                //                      ----+

                RFstate = TRFUNO;
                RX_Buffer[Bptr] >>= 1; // Rotate
                if (RFcount >= 0) {
                    RX_Buffer[Bptr] += 0x80; // Shift in bit
                }
                RFcount = 0; // Reset length counter
                High_Time = 0;

                if ((++BitCount & 7) == 0)
                    Bptr++; // Advance one byte
                if (BitCount == SX1239RAW_MAXBIT) // maximum number of bits
                {
                    RFstate = TRFDATAREADY; // Finished receiving
                }
            } else { // still low
                RFcount++;
                if (RFcount >= LOW_TO) // too long low
                { // If timeout, then check for what type of transmission detected.

                    RX_Buffer[Bptr] >>= 1; // Rotate

                    if (High_Time <= MIDPOINT) //shift in last receive bit
                    {
                        RX_Buffer[Bptr] += 0x80; // Shift in bit
                    }

                    if (BitCount < SX1239RAW_MINBIT) // Not a valid complete transmission so reset state machine
                    {
                        //UART_SendString("Low Reset!");
                        RFstate = TRFSYNC;
                        Bptr = 0;
                        BitCount = 0;
                        break;
                    }

                    RFstate = TRFDATAREADY;
                }
            }

            break;

        case TRFSYNC: // Expect header before looking for bits
            if (PICtail_RAW_DATA)
            {   // rising edge detected  	+---+                  +---..
                //                              |   |  <-Theader-> |
                //                              +------------------+

                if ((RFcount < SHORT_HEAD) || (RFcount >= LONG_HEAD)) {
                    RFstate = TRFreset;
                    RFcount = 0;
                    break; // too short/long, no header
                } else {
                    RFcount = 0; // restart counter
                    RFstate = TRFUNO; // Header detected, start reading bits
                }
            } else { // still low
                RFcount++;
            }
            break;


        case TRFSYNC0: // Wait for high level before looking for synch header
            if (RFsynch == 0) // High level detected?
            {
                if (PICtail_RAW_DATA) // High level detected reset counters and wait for sync header
                {
                    RFsynch = 1;
                } //RFbit
            }//RFsynch
            else {
                if (PICtail_RAW_DATA == 0) // low level synch pulse is probably detected
                {
                    RFstate = TRFSYNC;
                    RFsynch = 0;
                }
            }
            break;


        case TRFreset: // Reset state machine
        default:
            RFstate = TRFSYNC0; // reset state machine in all other cases
            RFcount = 0;
            Bptr = 0;
            BitCount = 0;
            RFsynch = 0;
            break;

    } // switch
} // rxi

void InitReceiver(void)
{
    //pictail_init();
    //sx1239_init();

    	
	//GPIOA_PIDR &= ~(1<<DATA2);
	//GPIOA_PDDR &= ~(1<<DATA2);
    // SX1239 demodulated output as input to PIC
    /*TRIS_PICtail_RAW_DATA = 1;

    TMR0 = 0;

    OPTION_REGbits.TMR0CS = 0;  // Clock from system clock

    INTCONbits.T0IF = 0;
    INTCONbits.T0IE = 1;        // Enable TMR1 overflow interrupt

    OPTION_REG = 0x00;*/

}

