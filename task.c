#include "task.h"
#include "io.h"
#include "hardware.h"
#include "keeloq_ultimate.h"
#include "encoder_mediator.h"
#include "tick.h"
#include "encoder_types.h"
#include "adc.h"
#include "uart.h"
#include "math.h"

extern bool LEARN_BUTTON_FLAG, IN1_FLAG, IN2_FLAG, IN3_FLAG, IN4_FLAG, IN5_FLAG, IN6_FLAG;
main_state_t main_state = NORMAL_MODE;
Timeout_Type learn_timeout;
ultimate_encoder_t Ultimate_Result;
extern ultimate_encoder_t ultimate_data;
control_state_t control_state = PAUSE;
led_state_t led_state = LED_LOW;
buzzer_state_t buzzer_state = SILENT;
static uint32_t control_tick;
extern measure_result_t measure_result;
extern uint16_t led_time, buzzer_time;
extern uint16_t led_time_tick, buzzer_time_tick;
extern uint8_t led_repeat, buzzer_repeat;
bool up_flag = false, down_flag = false, emergency_flag = false;

void Set_LED_Blink(uint16_t timeLength, uint8_t repeatNumber) {
    led_time = timeLength;
    led_time_tick = timeLength;
    led_repeat = repeatNumber;
    led_state = LED_HIGH;
}

void Set_Buzzer_Beep(uint16_t timeLength, uint8_t repeatNumber) {
    buzzer_time = timeLength;
    buzzer_time_tick = timeLength;
    buzzer_repeat = repeatNumber;
    buzzer_state = SOUND;
}

void RF_Task(void) {
    switch (main_state) {
        case NORMAL_MODE:
            if (LEARN_BUTTON_FLAG == true) {
                main_state = LEARN_MODE;
                enmed_processEvent(LEARN_EVENT);
                InitTimeout(&learn_timeout, SYSTICK_TIME_SEC(15));
                Set_LED_Blink(500, 10);
            }
            break;
        case LEARN_MODE:
            if (validated_packet.result_code == LEARN_SUCCESS) {
                main_state = NORMAL_MODE;
                enmed_processEvent(RESET_EVENT);
                Set_Buzzer_Beep(100, 2);
            }
            if (CheckTimeout(&learn_timeout) == 0) {
                main_state = NORMAL_MODE;
                enmed_processEvent(LEARN_TIMOUT_EVENT);
                Set_Buzzer_Beep(100, 5);
            }
            break;
        default: break;
    }
}

void Keeloq_Task(void) {
    switch (validated_packet.type) {
        case KEELOQ_ULTIMATE:
            Ultimate_Result = ultimate_data;
            switch (validated_packet.result_code) {
                case NO_DATA:
                    break;
                case VALID_PACKET:
                    UART_SendChar('o');
                    if (main_state == LEARN_MODE) {
                        // Enter here after device is learned
                    } else {
                        switch (Ultimate_Result.fcode) {
                            case 4:
                                if (control_state != LOCKED)
                                {
                                    control_state = PAUSE;
                                    up_flag = true;
                                }
                                break;
                            case 2:
                                if (control_state != LOCKED)
                                {
                                    control_state = PAUSE;
                                    down_flag = true;
                                }
                                break;
                            case 1:
                                if (control_state != LOCKED)
                                    control_state = PAUSE;
                                break;
                            case 8:
                                if (control_state != LOCKED)
                                    control_state = LOCKED;
                                else control_state = UNLOCKED;
                                break;
                            default: break;
                        }
                    }
                    //Must always send the serial first in order to identify what row to
                    // update
                    break;
                case TX_LOW_VOLTAGE:
                    //UART_SendString("Low Battery!");
                    break;
                case MAC_ERROR:
                    //UART_SendString("AUTH CODE ERROR!");
                    break;
                case NOT_VALID_PACKET:
                    //UART_SendString("Not Valid Packet!");
                    break;
                case NOT_LEARNED:
                    //Set_LED_Blink(500, 3);
                    //UART_SendString("Not Learned!");
                    break;
                case SEED_RECEIVED:
                    //UART_SendString("Seed OK Phase 2");
                    break;
                case LEARN_SUCCESS:
                    //UART_SendString("Learn OK");
                    break;
                case NO_SPACE:
                    break;
                case DECRYPT_FAIL:
                    //UART_SendString("Decryption Fail!");
                    break;
                case RESYNC_REQ:
                    //UART_SendString("Resync Required!");
                    break;
                case ENCODER_TIMER_RESYNC:
                    //UART_SendString("Encoder Resync");
                    break;
                case CNT_OUT_OF_SYNC:
                    //UART_SendString("CNT RESYNC REQ!");
                    break;
                case NVM_WRITE_ERROR:
                    //UART_SendString("NVM Write Error!");
                    break;
            }
            break;
        default: break;
    }
}

void Control_Task(void) {
    if (stuck_flag == 1) {
        Set_Buzzer_Beep(200, 3);
        UART_SendChar('s');
        stuck_flag = 0;
    }
    if (IN1_FLAG == true)
    {
        emergency_flag = true;
    }
    switch (control_state) {
        case UP:
            RELAY1 = RELAY_ON;
            RELAY2 = RELAY_ON;
            if (control_tick == 0)
            {
                control_state = PAUSE;
                Set_Buzzer_Beep(100, 1);
            }
            else control_tick--;
            //Set_Buzzer_Beep(100, 1);
            break;
        case DOWN:
            RELAY1 = RELAY_ON;
            RELAY2 = RELAY_OFF;
            if (control_tick == 0)
            {
                control_state = PAUSE;
                Set_Buzzer_Beep(100, 1);
            }
            else control_tick--;
            break;
        case PAUSE:
            RELAY1 = RELAY_OFF;
            RELAY2 = RELAY_OFF;
            /*if (control_tick == 0) {
                RELAY1 = RELAY_OFF;
                RELAY2 = RELAY_OFF;
            } else {
                //if (control_tick == 1)
                    //Set_Buzzer_Beep(100, 1);
                control_tick--;
            }*/
            if (up_flag == true)
            {
                control_tick = 120000;
                control_state = UP;
                up_flag = false;
            }
            if (down_flag == true)
            {
                control_tick = 120000;
                control_state = DOWN;
                down_flag = false;
            }
            if (emergency_flag == true)
            {
                control_tick = 10000;
                control_state = UP;
                emergency_flag = false;
                Set_Buzzer_Beep(100, 5);
            }
            break;
        case LOCKED:
            break;
        case UNLOCKED:
            break;
        default: break;
    }
    /*
     * adc, input task
     *
     * insert below 
     */
    measure_result.voltage = sqrt(measure_result.voltage) * VOLTAGE_CONVERT_PARAM;
    measure_result.current = sqrt(measure_result.current) * CURRENT_CONVERT_PARAM;
}

void LED_Task(void) {
    switch (led_state) {
        case LED_HIGH:
            LED = LED_ON;
            if (led_time_tick == 0) {
                led_state = LED_LOW;
                led_time_tick = led_time;
            } else led_time_tick--;
            break;
        case LED_LOW:
            LED = LED_OFF;
            led_time_tick--;
            if ((led_time_tick == 0)&&(led_repeat != 0)) {
                led_state = LED_HIGH;
                led_time_tick = led_time;
                led_repeat--;
            } else if ((led_time_tick == 0)&&(led_repeat == 0)) {
                led_time_tick = 0;
                led_time = 0;
                led_state = LED_LOW;
            }
        default: break;
    }
}

void Buzzer_Task(void) {

    switch (buzzer_state) {
        case SOUND:
            BUZZER = BUZZER_ON;
            if (buzzer_time_tick == 0) {
                buzzer_state = SILENT;
                buzzer_time_tick = buzzer_time;
            } else buzzer_time_tick--;
            break;
        case SILENT:
            BUZZER = BUZZER_OFF;
            buzzer_time_tick--;
            if ((buzzer_time_tick == 0)&&(buzzer_repeat != 0)) {
                buzzer_state = SOUND;
                buzzer_time_tick = buzzer_time;
                buzzer_repeat--;
            } else if ((buzzer_time_tick == 0)&&(buzzer_repeat == 0)) {
                buzzer_state = SILENT;
                buzzer_time = 0;
                buzzer_time_tick = 0;
            } 
        default: break;
    }
}