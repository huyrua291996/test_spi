/*
 * tick.c
 *
 *  Created on: Feb 21, 2019
 *      Author: huyru
 */

/*
 * STICK.c
 *
 *  Created on: Feb 8, 2017
 *      Author: Trieu
 */
#include "tick.h"
#include <stdbool.h>
uint16_t sysTickTimeSec = 0;
uint32_t sysTickCounter = 0;

void SYSTICK_TASK(void)
{
	sysTickCounter++;
	/*if(sysTickCounter%999==0) 
	{
		sysTickTimeSec++;	
        readTime = true;
	}*/
    /*if (sysTickCounter%250==0)
    {
        syncTickCounter++;
    }*/
}



uint32_t SysTick_Get(void)
{
	return sysTickCounter;
}
/*uint32_t SYSTICK_GETSEC(void)
{
	return sysTickTimeSec;
}*/
uint32_t CheckTimeout(Timeout_Type *t)
{
	uint32_t u32temp,u32temp1;
	u32temp = t->start_time + t->timeout;
	if(u32temp != t->crc) 
		return 0;
	u32temp = SysTick_Get();
	t->crc = t->start_time + t->timeout;
	if(u32temp >= t->start_time)
		u32temp1 = u32temp - t->start_time;
	else
		u32temp1 = (0xFFFFFFFF - t->start_time) + u32temp;
	if(u32temp1 >= t->timeout) return 0;
	return (t->timeout - u32temp1);
}

/*void delay1us(void)
{
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
	NOP();
}

void delayus(uint16_t time)
{
	uint16_t i;
	for (i = 0; i < time; i++)
	{
		delay1us();
	}
}

void delayms(uint32_t time)
{
	uint32_t i;
	uint16_t j;
	for (i = 0; i < time; i++)
	{
		for (j = 0; j < 1000; j++)
			delay1us();
	}
}
*/
void TimerDelayms(uint32_t time)
{
	uint32_t start_time,current_time;
	start_time = SysTick_Get();
	while(1)
	{
		current_time = SysTick_Get();
		if(current_time >= start_time)
		{
			if((current_time - start_time) >= time) break;
		}
		else
		{
			if(((0xFFFFFFFF - start_time) + current_time) >= time) break;
		}
	}
}
void InitTimeout(Timeout_Type *t,uint32_t timeout)
{
	t->start_time = SysTick_Get();
	t->timeout = timeout;
	t->crc = t->start_time + t->timeout;
}


