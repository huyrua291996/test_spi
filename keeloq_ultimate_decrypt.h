/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */

#ifndef KEELOQ_ULTIMATE_DECRYPT_H
#define	KEELOQ_ULTIMATE_DECRYPT_H

#include <stdint.h>
#include "encoder_types.h"
//#include "type.h"

#define UltimateMACLength   4



/** Decryption key for the AES algorithm. */
extern uint8_t DKEY[16];

void UltimateDecrypt(uint8_t* packet);
void UltimateLoadManufCode(void);
void UltimateNormalKeyGen(uint32_t* serial);
void UltimateSeedKeyGen(uint8_t* seed);
bool keeloq_ultimate_check_mac (ultimate_encoder_t* data);

//bool DecCHK(classic_encoder_t* data);
//result_code_t ReqResync(uint16_t CurrentHop);
//result_code_t HopCHK(classic_encoder_t* data, classic_nvm_t* nvm);

void* getUltimateManufCode(void);
void setUltimateManufCode(const uint8_t* data);
void* getUltimateAuthKey(void);
void setUltimateAuthKey(const uint8_t* data);

#endif	/* KEELOQ_ULTIMATE_DECRYPT_H */

