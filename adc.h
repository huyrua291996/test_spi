/* 
 * File:   adc.h
 * Author: huyru
 *
 * Created on December 20, 2019, 11:16 AM
 */

#ifndef ADC_H
#define	ADC_H
/**
  Section: Included Files
*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <xc.h>

#ifdef	__cplusplus
extern "C" {
#endif
#define VOLTAGE_CONVERT_PARAM 0.5
#define CURRENT_CONVERT_PARAM 0.5
    typedef enum
    {
        VOLTAGE_MEASURE,
        CURRENT_MEASURE,
    } measure_state_t;
    
    typedef struct
    {
        float voltage;
        float current;
    } measure_result_t;
    void ADC_Init(void);
    void ADC_ISR(void);


#ifdef	__cplusplus
}
#endif

#endif	/* ADC_H */

