/**
  EEPROM SPI Header File

  Company:
    Microchip Technology Inc.

  File Name:
    eeprom_spi.h

  Summary:
    This is the header file containing the EEPROM SPI functions and constants.

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB� Code Configurator - v2.25.2
        Device            :  PIC16F1719
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.34
        MPLAB             :  MPLAB X v2.35 or v3.00
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */
#include "mcc_generated_files/spi1.h"
#include "hardware.h"
#include <xc.h>

#ifndef EEPROM_H
#define	EEPROM_H

#ifdef	__cplusplus
extern "C" {
#endif

#define CS_LAT CS

#define EEPROM_READ_EN      0x03                // read data from memory
#define EEPROM_WREN         0x06                // set the write enable latch
#define EEPROM_WRITE_EN     0x02                // write data to memory array
#define EEPROM_RDSR         0x05                // read STATUS register
#define _XTAL_FREQ 32000000
    void SPI_ByteWrite1(uint16_t addressBuffer, uint8_t byteData);
    uint8_t SPI_ByteRead1 (uint16_t addressBuffer);
    void SPI_ByteWrite(uint8_t *addressBuffer, uint8_t addlen, uint8_t byteData);
    uint8_t SPI_ByteRead(uint8_t *addressBuffer, uint8_t addlen);
    uint8_t SPI1_ReadStatusRegister(void);
    uint8_t SPI_WritePoll(void);
    void SPI_SequentialWrite(uint8_t *addressBuffer, uint8_t addlen, uint8_t *writeBuffer, uint8_t buflen);
    void SPI_SequentialRead(uint8_t *addressBuffer, uint8_t addlen, uint8_t *readBuffer, uint8_t buflen);
    void SPI_SequentialWrite1(uint16_t addressBuffer, uint8_t *writeBuffer, uint8_t buflen);
    void SPI_SequentialRead1(uint16_t addressBuffer, uint8_t *readBuffer, uint8_t buflen);
    uint8_t fread(void * ptr, uint8_t size, uint16_t *position) ;
    uint8_t fwrite(void * ptr, uint8_t size, uint16_t *position);

#ifdef	__cplusplus
}
#endif

#endif	/* EEPROM_SPI_H */

