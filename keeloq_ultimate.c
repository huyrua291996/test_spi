/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */

#include "eeprom.h"


//#ifdef __XC8
//#include <xc.h>
//#else
//#include <pic.h>
//#endif
#include <stdbool.h>
#include <stdint.h>
//#include <stdlib.h>
//#include <string.h>
#include <stdio.h>
#include "keeloq_ultimate.h"
#include "keeloq_ultimate_decrypt.h"
#include "nvm_mediator.h"
#include "sync_timer.h"
#include "task.h"
#include "uart.h"


#define ULTIMATE_RESYNCH_RECEICER_ON_TRANSMITTER

ultimate_state_t ultimate_mode = ULTIMATE_NORMAL_MODE;
ultimate_secure_learn_state_t ultimate_secure_learn_state = ULTIMATE_SECURE_LEARN_SEED;

extern int32_t counter_number;
extern uint16_t packet;
uint8_t tempSeed[16];
//uint8_t info[150];
extern uint8_t stuck_flag;
//#ifdef __XC8
ultimate_encoder_t ultimate_data;
//#endif

ultimate_nvm_t ultimate_nvm;
rawdata_packet_t tmpPacket;

int32_t timerRx;
int32_t timerTx;
int32_t timerDelta;
uint32_t timerWindow;
uint8_t file;
int32_t deltaStored;
int32_t timerRxRecalc;
uint16_t reSyncCounter;
uint24_t sync;
static uint8_t stuck = 0;
static uint24_t resync = 0; //this is resync for sync counter

uint8_t Decode_Ultimate_KeeLoq(rawdata_packet_t* inputData) {

    //#ifndef __XC8
    //ultimate_encoder_t ultimate_data;
    //#endif
    //validated_packet_t validated_packet;


    //Workaround for compiler bug that looses track of the bank when passing pointers
    memcpy(&tmpPacket, inputData, sizeof (rawdata_packet_t));

    if (tmpPacket.BitCount != 191) {
        // Not a KeeLoq Ultimate Packet
        validated_packet.result_code = NOT_VALID_PACKET;
        //sprintf(info, "%d Bit Packet!", tmpPacket.BitCount);
        //UART_SendString(info);
        return (uint8_t) validated_packet.result_code;
    }

    //Set Encoder Type

    //UART_SendString("Found Signal!");
    //Load Packet in Struct
    ultimate_data.mac = ((uint32_t) (tmpPacket.tmpBuffer[3]) << 24) | ((uint32_t) (tmpPacket.tmpBuffer[2]) << 16);
    ultimate_data.mac |= ((uint32_t) (tmpPacket.tmpBuffer[1]) << 8) | ((uint32_t) (tmpPacket.tmpBuffer[0]));
    ultimate_data.serialnumber = ((uint32_t) (tmpPacket.tmpBuffer[23]) << 24) | ((uint32_t) (tmpPacket.tmpBuffer[22]) << 16);
    ultimate_data.serialnumber |= ((uint32_t) (tmpPacket.tmpBuffer[21]) << 8) | ((uint32_t) (tmpPacket.tmpBuffer[20]));
    ultimate_data.resync = ((uint16_t) tmpPacket.tmpBuffer[5] << 8) | ((uint16_t) tmpPacket.tmpBuffer[4]);
    ultimate_data.btn_timer = ((uint16_t) tmpPacket.tmpBuffer[7] << 8) | ((uint16_t) tmpPacket.tmpBuffer[6]);
    ultimate_data.ls_timer = ((int32_t) tmpPacket.tmpBuffer[11] << 24) | ((int32_t) tmpPacket.tmpBuffer[10] << 16);
    ultimate_data.ls_timer |= ((int32_t) tmpPacket.tmpBuffer[9] << 8) | ((int32_t) tmpPacket.tmpBuffer[8]);
    ultimate_data.fcode = (tmpPacket.tmpBuffer[12]);
    ultimate_data.batt_level = (tmpPacket.tmpBuffer[13]);
    ultimate_data.sync = ((uint24_t) tmpPacket.tmpBuffer[16] << 16) | ((uint24_t) tmpPacket.tmpBuffer[15] << 8) | ((uint24_t) tmpPacket.tmpBuffer[14]);
    ultimate_data.delta_time = ((uint24_t) tmpPacket.tmpBuffer[19] << 16) | ((uint24_t) tmpPacket.tmpBuffer[18] << 8) | ((uint24_t) tmpPacket.tmpBuffer[17]);

    memcpy(ultimate_data.raw_data, &tmpPacket.tmpBuffer[4], 16); //Copies encrypted data into raw_data for later

    memcpy(validated_packet.data, &ultimate_data, sizeof (ultimate_encoder_t)); //Populate serial number
    if (ultimate_data.serialnumber == 0xFFFFFFFF) {
        ultimate_mode = ULTIMATE_LEARN;
        Set_Buzzer_Beep(200, 2);
        validated_packet.result_code = ENCODER_TIMER_RESYNC;
        return (uint8_t) validated_packet.result_code;
    }
    validated_packet.type = KEELOQ_ULTIMATE;
    switch (ultimate_mode) {
        case ULTIMATE_NORMAL_MODE:

            for (file = 0; file < 3; file++) {
                //Verify Serial Number
                fopen(file);
                //look for serial number
                if (find(&ultimate_data.serialnumber) == false) {
                    //No Match, Transmitter Not Learned
                    validated_packet.result_code = NOT_LEARNED;
                    continue;
                }
                // Check MAC signature
                if (keeloq_ultimate_check_mac(&ultimate_data) == false) {
                    validated_packet.result_code = MAC_ERROR;
                    break;
                }
                /*
                // just ignore the seed transmission during normal mode
         
                //Read out memory
                ultimate_nvm = (ultimate_nvm_t*) read();

                //Copy seed from nvm
                for (int i = 0; i < 16; i++) {
                    ultimate_data.seed[i] = ultimate_nvm->seed[i];
                }*/

                //Decrypt and load data
                keeloq_ultimate_decrypt_packet(&ultimate_data, &tmpPacket);
                //extract_decrypted_data(tmpPacket);
                //ultimate_data.mac = (tmpPacket.tmpBuffer[3] << 24)|(tmpPacket.tmpBuffer[2]<<16)|(tmpPacket.tmpBuffer[1]<<8)|(tmpPacket.tmpBuffer[0]);
                ultimate_data.resync = ((uint16_t) tmpPacket.tmpBuffer[5] << 8) | ((uint16_t) tmpPacket.tmpBuffer[4]);
                ultimate_data.btn_timer = ((uint16_t) tmpPacket.tmpBuffer[7] << 8) | ((uint16_t) tmpPacket.tmpBuffer[6]);
                ultimate_data.ls_timer = ((uint32_t) tmpPacket.tmpBuffer[11] << 24) | ((uint32_t) tmpPacket.tmpBuffer[10] << 16);
                ultimate_data.ls_timer |= ((uint32_t) tmpPacket.tmpBuffer[9] << 8) | ((uint32_t) tmpPacket.tmpBuffer[8]);
                ultimate_data.fcode = (tmpPacket.tmpBuffer[12]);
                ultimate_data.batt_level = (tmpPacket.tmpBuffer[13]);
                ultimate_data.sync = ((uint24_t) tmpPacket.tmpBuffer[16] << 16) | ((uint24_t) tmpPacket.tmpBuffer[15] << 8) | ((uint24_t) tmpPacket.tmpBuffer[14]);
                ultimate_data.delta_time = ((uint24_t) tmpPacket.tmpBuffer[19] << 16) | ((uint24_t) tmpPacket.tmpBuffer[18] << 8) | ((uint24_t) tmpPacket.tmpBuffer[17]);

                //sprintf(info, "batt: %x, sync: %08x", ultimate_data.batt_level, ultimate_data.sync);
                //UART_SendString(info);
                //ultimate_data.serialnumber = (tmpPacket.tmpBuffer[23] << 24)|(tmpPacket.tmpBuffer[22]<<16)|(tmpPacket.tmpBuffer[21]<<8)|(tmpPacket.tmpBuffer[20]);
                // check the sync counter
                ultimate_nvm.sync = ((uint24_t) currentRecord[14] << 16) | ((uint24_t) currentRecord[13] << 8) | ((uint24_t) currentRecord[12]);
                sync = ultimate_nvm.sync;
                ultimate_nvm.delta = ((int32_t) currentRecord[7] << 24) | ((int32_t) currentRecord[6] << 16);
                ultimate_nvm.delta |= ((int32_t) currentRecord[5] << 8) | ((int32_t) currentRecord[4]);
                ultimate_nvm.resync = ((uint16_t) currentRecord[16] << 8) | ((uint16_t) currentRecord[15]);
                ultimate_nvm.timestamp = ((int32_t) currentRecord[11] << 24) | ((int32_t) currentRecord[10] << 16);
                ultimate_nvm.timestamp |= ((int32_t) currentRecord[9] << 8) | ((int32_t) currentRecord[8]);
                ultimate_nvm.serial = ((uint32_t) currentRecord[3] << 24) | ((uint32_t) currentRecord[2] << 16);
                ultimate_nvm.serial |= ((uint32_t) currentRecord[1] << 8) | ((uint32_t) currentRecord[0]);
                ultimate_nvm.sync_timestamp = ((int32_t) currentRecord[20] << 24) | ((int32_t) currentRecord[19] << 16);
                ultimate_nvm.sync_timestamp |= ((int32_t) currentRecord[18] << 8) | ((int32_t) currentRecord[17]);
                if (ultimate_data.sync < sync) {
                    // counter has not advanced or is in the past
                    validated_packet.result_code = CNT_OUT_OF_SYNC;
                    Set_LED_Blink(200, 1);
                    break;
                }

                if (ultimate_data.sync == sync) {
                    stuck++;
                    if (stuck == 10) {
                        stuck_flag = 1;
                        stuck = 0;
                    }
                }

                if ((ultimate_data.sync - sync) > COUNTER_MAX_ADVANCE_ALLOWED) {
                    // timer difference is too large
                    // check for resync
                    //Set_LED_Blink(200,2);
                    if (ultimate_data.sync != (resync + 1)) {
                        // store value for resync
                        resync = ultimate_data.sync;
                        //Set_LED_Blink(200, 2);
                        validated_packet.result_code = CNT_OUT_OF_SYNC;
                        break;
                    }
                }
                getTimer();
                // snapshot timer Rx
                timerRx = counter_number - ultimate_nvm.sync_timestamp;
                ultimate_data.rx_timestamp = timerRx; //store for p2p

                // get timestamp from received packet
                timerTx = ultimate_data.ls_timer;
                // get delta from the eeprom corresponding to current serial
                deltaStored = ultimate_nvm.delta;
                // estimate the receiver timer value from the delta and tx timestamp
                timerRxRecalc = timerTx + deltaStored;
                // evaluate the time difference between the two timers
                if (timerRx >= timerRxRecalc)
                    timerDelta = timerRx - timerRxRecalc;
                if (timerRxRecalc > timerRx)
                    timerDelta = timerRxRecalc - timerRx;

                // see if we have a resynch event on the transmitter
                reSyncCounter = ultimate_nvm.resync;

                // implement an adaptive window based on timerDelta, timeStamp and lastTimeStamp

                // Adaptive Window TimerDelta must be within 6.25%
                timerWindow = (timerRx - deltaStored - ultimate_nvm.timestamp);
                timerWindow = (timerWindow >> 4) + 0x0F;
                //sprintf(info, "timer delta: %ld, timer Window: %ld, rx_timestamp: %ld, delta_store: %ld, timerRx: %ld, timerTx: %ld, syncTime: %ld",
                //        timerDelta, timerWindow, ultimate_nvm.timestamp, deltaStored, timerRx, timerTx, ultimate_nvm.sync_timestamp);
                //UART_SendString(info);
                if (timerDelta < timerWindow) {

                    // Save the timestamp for each encoder
                    ultimate_nvm.timestamp = ultimate_data.ls_timer;

                    // update the counter
                    ultimate_nvm.sync = ultimate_data.sync;

                    //Save the deltaT(Tx) to the EEPROM
                    ultimate_nvm.delta = timerRx - timerTx;
                    //transfer_to_record(ultimate_nvm);
                    currentRecord[0] = (uint8_t) (ultimate_nvm.serial & 0xff);
                    currentRecord[1] = (uint8_t) ((ultimate_nvm.serial >> 8) & 0xff);
                    currentRecord[2] = (uint8_t) ((ultimate_nvm.serial >> 16) & 0xff);
                    currentRecord[3] = (uint8_t) ((ultimate_nvm.serial >> 24) & 0xff);
                    currentRecord[4] = (uint8_t) ((ultimate_nvm.delta >> 0) & 0xff);
                    currentRecord[5] = (uint8_t) ((ultimate_nvm.delta >> 8) & 0xff);
                    currentRecord[6] = (uint8_t) ((ultimate_nvm.delta >> 16) & 0xff);
                    currentRecord[7] = (uint8_t) ((ultimate_nvm.delta >> 24) & 0xff);
                    currentRecord[8] = (uint8_t) ((ultimate_nvm.timestamp >> 0) & 0xff);
                    currentRecord[9] = (uint8_t) ((ultimate_nvm.timestamp >> 8) & 0xff);
                    currentRecord[10] = (uint8_t) ((ultimate_nvm.timestamp >> 16) & 0xff);
                    currentRecord[11] = (uint8_t) ((ultimate_nvm.timestamp >> 24) & 0xff);
                    currentRecord[12] = (uint8_t) ((ultimate_nvm.sync >> 0) & 0xff);
                    currentRecord[13] = (uint8_t) ((ultimate_nvm.sync >> 8) & 0xff);
                    currentRecord[14] = (uint8_t) ((ultimate_nvm.sync >> 16) & 0xff);
                    currentRecord[15] = (uint8_t) (ultimate_nvm.resync & 0xff);
                    currentRecord[16] = (uint8_t) ((ultimate_nvm.resync >> 8) & 0xff);
                    currentposition = (uint16_t) systemFiles[file].start;
                    Set_LED_Blink(200, 3);
                    //Write back to NVM
                    write();

                    // Copy decrypted data to output
                    memcpy(validated_packet.data, &ultimate_data, sizeof (ultimate_encoder_t));
                    // write result code
                    validated_packet.result_code = VALID_PACKET;
                    memset(currentRecord, 0, sizeof (currentRecord));
                    break;
                    // Test low voltage indicator
                    /*if (ultimate_data.batt_level & 0x80)
                        validated_packet.result_code = TX_LOW_VOLTAGE;*/

                }// end of "if ( window(timerDelta......."
                else {
                    validated_packet.result_code = ENCODER_TIMER_RESYNC;

                    // check if the re-sync counter has increased by a small number (battery removal)
                    // Allow a window for resync
                    if ((ultimate_data.resync - reSyncCounter) < 10) {
                        if (ultimate_data.resync == reSyncCounter) {
                            Set_LED_Blink(200, 5);
                            break;
                        }

                        timerDelta = timerRx - timerTx;
                        //Save the deltaT(Tx) to the EEPROM
                        ultimate_nvm.delta = timerDelta;
                        //Save the new re-sync counter
                        ultimate_nvm.resync = ultimate_data.resync;
                        //transfer_to_record(ultimate_nvm);
                        currentRecord[0] = (uint8_t) (ultimate_nvm.serial & 0xff);
                        currentRecord[1] = (uint8_t) ((ultimate_nvm.serial >> 8) & 0xff);
                        currentRecord[2] = (uint8_t) ((ultimate_nvm.serial >> 16) & 0xff);
                        currentRecord[3] = (uint8_t) ((ultimate_nvm.serial >> 24) & 0xff);
                        currentRecord[4] = (uint8_t) ((ultimate_nvm.delta >> 0) & 0xff);
                        currentRecord[5] = (uint8_t) ((ultimate_nvm.delta >> 8) & 0xff);
                        currentRecord[6] = (uint8_t) ((ultimate_nvm.delta >> 16) & 0xff);
                        currentRecord[7] = (uint8_t) ((ultimate_nvm.delta >> 24) & 0xff);
                        currentRecord[8] = (uint8_t) ((ultimate_nvm.timestamp >> 0) & 0xff);
                        currentRecord[9] = (uint8_t) ((ultimate_nvm.timestamp >> 8) & 0xff);
                        currentRecord[10] = (uint8_t) ((ultimate_nvm.timestamp >> 16) & 0xff);
                        currentRecord[11] = (uint8_t) ((ultimate_nvm.timestamp >> 24) & 0xff);
                        currentRecord[12] = (uint8_t) ((ultimate_nvm.sync >> 0) & 0xff);
                        currentRecord[13] = (uint8_t) ((ultimate_nvm.sync >> 8) & 0xff);
                        currentRecord[14] = (uint8_t) ((ultimate_nvm.sync >> 16) & 0xff);
                        currentRecord[15] = (uint8_t) (ultimate_nvm.resync & 0xff);
                        currentRecord[16] = (uint8_t) ((ultimate_nvm.resync >> 8) & 0xff);
                        currentposition = (uint16_t) systemFiles[file].start;
                        //Write back to NVM
                        write();
                        Set_LED_Blink(200, 4);
                        // write result code
                        validated_packet.result_code = ENCODER_TIMER_RESYNC;
                        break;
                    }

                }
            }
            break;
        case ULTIMATE_LEARN:

            // Check MAC signature
            if (keeloq_ultimate_check_mac(&ultimate_data) == false) {
                validated_packet.result_code = MAC_ERROR;
                break;
            }

            //Decrypt and load data
            keeloq_ultimate_decrypt_packet(&ultimate_data, &tmpPacket);
            //extract_decrypted_data(tmpPacket);
            ultimate_data.resync = ((uint16_t) tmpPacket.tmpBuffer[5] << 8) | ((uint16_t) tmpPacket.tmpBuffer[4]);
            ultimate_data.btn_timer = ((uint16_t) tmpPacket.tmpBuffer[7] << 8) | ((uint16_t) tmpPacket.tmpBuffer[6]);
            ultimate_data.ls_timer = ((uint32_t) tmpPacket.tmpBuffer[11] << 24) | ((uint32_t) tmpPacket.tmpBuffer[10] << 16);
            ultimate_data.ls_timer |= ((uint32_t) tmpPacket.tmpBuffer[9] << 8) | ((uint32_t) tmpPacket.tmpBuffer[8]);
            ultimate_data.fcode = (tmpPacket.tmpBuffer[12]);
            ultimate_data.batt_level = (tmpPacket.tmpBuffer[13]);
            ultimate_data.sync = ((uint24_t) tmpPacket.tmpBuffer[16] << 16) | ((uint24_t) tmpPacket.tmpBuffer[15] << 8) | ((uint24_t) tmpPacket.tmpBuffer[14]);
            ultimate_data.delta_time = ((uint24_t) tmpPacket.tmpBuffer[19] << 16) | ((uint24_t) tmpPacket.tmpBuffer[18] << 8) | ((uint24_t) tmpPacket.tmpBuffer[17]);

            //Normal Learn
            for (file = 0; file < 3; file++) {
                fopen(file);
                if (find(&ultimate_data.serialnumber) == true) { //look for serial number for relearn
                    //No Match, Add Serial Number
                    ultimate_nvm.serial = ultimate_data.serialnumber;
                    //Read out memory
                    //ultimate_nvm = (ultimate_nvm_t*) read();


                    ultimate_nvm.sync_timestamp = ((int32_t) currentRecord[20] << 24) | ((int32_t) currentRecord[19] << 16);
                    ultimate_nvm.sync_timestamp |= ((int32_t) currentRecord[18] << 8) | ((int32_t) currentRecord[17]);

                    //Copy data to save to nvm

                    timerTx = ultimate_data.ls_timer;
                    getTimer();
                    // snapshot timer Rx
                    timerRx = counter_number - ultimate_nvm.sync_timestamp;
                    ultimate_data.rx_timestamp = timerRx; //store for p2p

                    timerDelta = timerRx - timerTx;
                    //Save the deltaT(Tx) to the EEPROM
                    ultimate_nvm.delta = 0;
                    //Save the timestamp
                    ultimate_nvm.timestamp = timerTx;
                    //save counter
                    ultimate_nvm.sync = ultimate_data.sync;
                    //save re-sync counter
                    ultimate_nvm.resync = ultimate_data.resync;

                    //memset(ultimate_nvm.seed, 0, 16); //Set Seed to zero
                    //strncpy(currentRecord, &ultimate_nvm, 14);
                    //transfer_to_record(ultimate_nvm);
                    currentRecord[0] = (uint8_t) (ultimate_nvm.serial & 0xff);
                    currentRecord[1] = (uint8_t) ((ultimate_nvm.serial >> 8) & 0xff);
                    currentRecord[2] = (uint8_t) ((ultimate_nvm.serial >> 16) & 0xff);
                    currentRecord[3] = (uint8_t) ((ultimate_nvm.serial >> 24) & 0xff);
                    currentRecord[4] = (uint8_t) ((ultimate_nvm.delta >> 0) & 0xff);
                    currentRecord[5] = (uint8_t) ((ultimate_nvm.delta >> 8) & 0xff);
                    currentRecord[6] = (uint8_t) ((ultimate_nvm.delta >> 16) & 0xff);
                    currentRecord[7] = (uint8_t) ((ultimate_nvm.delta >> 24) & 0xff);
                    currentRecord[8] = (uint8_t) ((ultimate_nvm.timestamp >> 0) & 0xff);
                    currentRecord[9] = (uint8_t) ((ultimate_nvm.timestamp >> 8) & 0xff);
                    currentRecord[10] = (uint8_t) ((ultimate_nvm.timestamp >> 16) & 0xff);
                    currentRecord[11] = (uint8_t) ((ultimate_nvm.timestamp >> 24) & 0xff);
                    currentRecord[12] = (uint8_t) ((ultimate_nvm.sync >> 0) & 0xff);
                    currentRecord[13] = (uint8_t) ((ultimate_nvm.sync >> 8) & 0xff);
                    currentRecord[14] = (uint8_t) ((ultimate_nvm.sync >> 16) & 0xff);
                    currentRecord[15] = (uint8_t) (ultimate_nvm.resync & 0xff);
                    currentRecord[16] = (uint8_t) ((ultimate_nvm.resync >> 8) & 0xff);
                    currentposition = (uint16_t) systemFiles[file].start;
                    //Write back to NVM
                    write();

                    //Exit Learn mode
                    ultimate_mode = ULTIMATE_NORMAL_MODE;
                    validated_packet.result_code = LEARN_SUCCESS;
                    break;
                } else if (((currentRecord[0] == 0)&&(currentRecord[1] == 0)&&(currentRecord[2] == 0)&&(currentRecord[3] == 0))
                        || ((currentRecord[0] == 0xff)&&(currentRecord[1] == 0xff)&&(currentRecord[2] == 0xff)&&(currentRecord[3] == 0xff))) {
                    ultimate_nvm.serial = ultimate_data.serialnumber;
                    //Read out memory
                    //ultimate_nvm = (ultimate_nvm_t*) read();



                    //Copy data to save to nvm

                    timerTx = ultimate_data.ls_timer;
                    getTimer();
                    // snapshot timer Rx
                    timerRx = counter_number;
                    ultimate_data.rx_timestamp = timerRx; //store for p2p

                    timerDelta = timerRx - timerTx;
                    //Save the deltaT(Tx) to the EEPROM
                    ultimate_nvm.delta = 0;
                    ultimate_nvm.sync_timestamp = timerDelta;
                    //Save the timestamp
                    ultimate_nvm.timestamp = timerTx;
                    //save counter
                    ultimate_nvm.sync = ultimate_data.sync;
                    //save re-sync counter
                    ultimate_nvm.resync = ultimate_data.resync;

                    //memset(ultimate_nvm.seed, 0, 16); //Set Seed to zero
                    //strncpy(currentRecord, &ultimate_nvm, 14);
                    //transfer_to_record(ultimate_nvm);
                    currentRecord[0] = (uint8_t) (ultimate_nvm.serial & 0xff);
                    currentRecord[1] = (uint8_t) ((ultimate_nvm.serial >> 8) & 0xff);
                    currentRecord[2] = (uint8_t) ((ultimate_nvm.serial >> 16) & 0xff);
                    currentRecord[3] = (uint8_t) ((ultimate_nvm.serial >> 24) & 0xff);
                    currentRecord[4] = (uint8_t) ((ultimate_nvm.delta >> 0) & 0xff);
                    currentRecord[5] = (uint8_t) ((ultimate_nvm.delta >> 8) & 0xff);
                    currentRecord[6] = (uint8_t) ((ultimate_nvm.delta >> 16) & 0xff);
                    currentRecord[7] = (uint8_t) ((ultimate_nvm.delta >> 24) & 0xff);
                    currentRecord[8] = (uint8_t) ((ultimate_nvm.timestamp >> 0) & 0xff);
                    currentRecord[9] = (uint8_t) ((ultimate_nvm.timestamp >> 8) & 0xff);
                    currentRecord[10] = (uint8_t) ((ultimate_nvm.timestamp >> 16) & 0xff);
                    currentRecord[11] = (uint8_t) ((ultimate_nvm.timestamp >> 24) & 0xff);
                    currentRecord[12] = (uint8_t) ((ultimate_nvm.sync >> 0) & 0xff);
                    currentRecord[13] = (uint8_t) ((ultimate_nvm.sync >> 8) & 0xff);
                    currentRecord[14] = (uint8_t) ((ultimate_nvm.sync >> 16) & 0xff);
                    currentRecord[15] = (uint8_t) (ultimate_nvm.resync & 0xff);
                    currentRecord[16] = (uint8_t) ((ultimate_nvm.resync >> 8) & 0xff);
                    currentRecord[17] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 0) & 0xff);
                    currentRecord[18] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 8) & 0xff);
                    currentRecord[19] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 16) & 0xff);
                    currentRecord[20] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 24) & 0xff);
                    currentposition = (uint16_t) systemFiles[file].start;
                    //Write back to NVM
                    write();

                    //Exit Learn mode
                    ultimate_mode = ULTIMATE_NORMAL_MODE;
                    validated_packet.result_code = LEARN_SUCCESS;
                    break;
                }
                /*if (add_serial(&ultimate_data.serialnumber) == 0) {
                    //Can't Add: No Space
                    validated_packet.result_code = NO_SPACE;
                    break;
                }*/
                //ultimate_nvm->serial = ultimate_data.serialnumber;
                //Point to beginning of block record
                /*if (find(&ultimate_data.serialnumber) == false) {
                    //NVM Write Error
                    validated_packet.result_code = NVM_WRITE_ERROR;
                    break;
                }*/
                //}
            }
            break;

        case ULTIMATE_RESYNC:
            keeloq_ultimate_decrypt_packet(&ultimate_data, &tmpPacket);
            //extract_decrypted_data(tmpPacket);
            ultimate_data.resync = ((uint16_t) tmpPacket.tmpBuffer[5] << 8) | ((uint16_t) tmpPacket.tmpBuffer[4]);
            ultimate_data.btn_timer = ((uint16_t) tmpPacket.tmpBuffer[7] << 8) | ((uint16_t) tmpPacket.tmpBuffer[6]);
            ultimate_data.ls_timer = ((uint32_t) tmpPacket.tmpBuffer[11] << 24) | ((uint32_t) tmpPacket.tmpBuffer[10] << 16);
            ultimate_data.ls_timer |= ((uint32_t) tmpPacket.tmpBuffer[9] << 8) | ((uint32_t) tmpPacket.tmpBuffer[8]);
            ultimate_data.fcode = (tmpPacket.tmpBuffer[12]);
            ultimate_data.batt_level = (tmpPacket.tmpBuffer[13]);
            ultimate_data.sync = ((uint24_t) tmpPacket.tmpBuffer[16] << 16) | ((uint24_t) tmpPacket.tmpBuffer[15] << 8) | ((uint24_t) tmpPacket.tmpBuffer[14]);
            ultimate_data.delta_time = ((uint24_t) tmpPacket.tmpBuffer[19] << 16) | ((uint24_t) tmpPacket.tmpBuffer[18] << 8) | ((uint24_t) tmpPacket.tmpBuffer[17]);
            // The receiver has lost track of time due to a power loss
#ifdef ULTIMATE_RESYNCH_RECEICER_ON_TRANSMITTER
            // get timestamp from received packet
            timerTx = ultimate_data.ls_timer;
            // get delta from the eeprom corresponding to current SN
            if (find(&ultimate_data.serialnumber) == true) {
                //ultimate_nvm.delta = ((int32_t) currentRecord[7] << 24) | ((int32_t) currentRecord[6] << 16) | ((int32_t) currentRecord[5] << 8) | ((int32_t) currentRecord[4]);
                //Read out memory
                //ultimate_nvm = (ultimate_nvm_t*) read();
                //deltaStored = ultimate_nvm.delta;
                // estimate the receiver timer value from the delta and tx timestamp
                //timerRxRecalc = timerTx + deltaStored;
                // evaluate the time difference between the two timers
                // Since the receiver timer not ticking (power loss)
                // the receiver time will always lag, we calculate delta the other way around
                //timerDelta = timerRxRecalc - timerRx;

                // Allow a window of aproximately one day
                //if (timerDelta < 45000) {
                // update the receiver time
                /*Sync Timer RTC*/
                ultimate_nvm.serial = ultimate_data.serialnumber;
                getTimer();
                ultimate_nvm.sync_timestamp = counter_number - timerTx;
                ultimate_nvm.delta = 0;
                //Save the timestamp
                ultimate_nvm.timestamp = timerTx;
                //save counter
                ultimate_nvm.sync = ultimate_data.sync;
                //save re-sync counter
                ultimate_nvm.resync = ultimate_data.resync;
                //transfer_to_record(ultimate_nvm);
                currentRecord[0] = (uint8_t) (ultimate_nvm.serial & 0xff);
                currentRecord[1] = (uint8_t) ((ultimate_nvm.serial >> 8) & 0xff);
                currentRecord[2] = (uint8_t) ((ultimate_nvm.serial >> 16) & 0xff);
                currentRecord[3] = (uint8_t) ((ultimate_nvm.serial >> 24) & 0xff);
                currentRecord[4] = (uint8_t) ((ultimate_nvm.delta >> 0) & 0xff);
                currentRecord[5] = (uint8_t) ((ultimate_nvm.delta >> 8) & 0xff);
                currentRecord[6] = (uint8_t) ((ultimate_nvm.delta >> 16) & 0xff);
                currentRecord[7] = (uint8_t) ((ultimate_nvm.delta >> 24) & 0xff);
                currentRecord[8] = (uint8_t) ((ultimate_nvm.timestamp >> 0) & 0xff);
                currentRecord[9] = (uint8_t) ((ultimate_nvm.timestamp >> 8) & 0xff);
                currentRecord[10] = (uint8_t) ((ultimate_nvm.timestamp >> 16) & 0xff);
                currentRecord[11] = (uint8_t) ((ultimate_nvm.timestamp >> 24) & 0xff);
                currentRecord[12] = (uint8_t) ((ultimate_nvm.sync >> 0) & 0xff);
                currentRecord[13] = (uint8_t) ((ultimate_nvm.sync >> 8) & 0xff);
                currentRecord[14] = (uint8_t) ((ultimate_nvm.sync >> 16) & 0xff);
                currentRecord[15] = (uint8_t) (ultimate_nvm.resync & 0xff);
                currentRecord[16] = (uint8_t) ((ultimate_nvm.resync >> 8) & 0xff);
                currentRecord[17] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 0) & 0xff);
                currentRecord[18] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 8) & 0xff);
                currentRecord[19] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 16) & 0xff);
                currentRecord[20] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 24) & 0xff);
                currentposition = (uint16_t) systemFiles[KEELOQ_ULTIMATE_FILE1].start;
                //Write back to NVM
                write();
                /*********/
                //setTimer(timerTx);
                ultimate_mode = ULTIMATE_NORMAL_MODE;
                //break;
                //}
                //ultimate_mode = ULTIMATE_RESYNC;
            } else {
                // Have not received a known transmitter, wait some more
                // until the re-synch timeout occours
                ultimate_mode = ULTIMATE_RESYNC;
                validated_packet.result_code = NOT_LEARNED;
            }

#endif
            break;

        case ULTIMATE_SECURE_LEARN:

            switch (ultimate_secure_learn_state) {
                case ULTIMATE_SECURE_LEARN_SEED:
                    //Check for seed transmission
                    if (ultimate_data.serialnumber == 0xFFFFFFFF) {
                        // copy the seed information
                        // seed information is from 'resync' to 'delta_time'
                        memcpy(tempSeed, (uint8_t *) & ultimate_data.resync, 16);
                        validated_packet.result_code = SEED_RECEIVED;
                        ultimate_secure_learn_state = ULTIMATE_SECURE_LEARN_NORMAL;
                    } else {
                        validated_packet.result_code = NOT_SEED_PACKET;
                    }
                    break;
                case ULTIMATE_SECURE_LEARN_NORMAL:
                    if (ultimate_data.serialnumber == 0xFFFFFFFF) {
                        validated_packet.result_code = SEED_RECEIVED;
                        ultimate_secure_learn_state = ULTIMATE_SECURE_LEARN_NORMAL;
                        break;
                    }

                    // Check MAC signature - Need serial number before we can do this
                    if (keeloq_ultimate_check_mac(&ultimate_data) == false) {
                        validated_packet.result_code = MAC_ERROR;
                        break;
                    }

                    // Generate key
                    UltimateSeedKeyGen(tempSeed);
                    UltimateDecrypt((uint8_t *) & ultimate_data.resync);

                    fopen(KEELOQ_ULTIMATE_FILE1);
                    if (find(&ultimate_data.serialnumber) == false) {
                        //No Match, Add Serial Number
                        if (add_serial(&ultimate_data.serialnumber) == 0) {
                            //Can't Add: No Space
                            validated_packet.result_code = NO_SPACE;
                            break;
                        }
                        //Point to beginning of block record
                        if (find(&ultimate_data.serialnumber) == false) {
                            //NVM Write Error
                            validated_packet.result_code = NVM_WRITE_ERROR;
                            break;
                        }
                    }
                    // now learn the encoder

                    //Read out memory
                    //ultimate_nvm = (ultimate_nvm_t*) read();

                    //Copy data to save to nvm

                    // save the local stored seed to NVM
                    memcpy(ultimate_nvm.seed, tempSeed, 16);

                    timerTx = ultimate_data.ls_timer;
                    getTimer();
                    // snapshot timer Rx
                    timerRx = counter_number;
                    ultimate_data.rx_timestamp = timerRx; //store for p2p

                    timerDelta = timerRx - timerTx;
                    //Save the deltaT(Tx) to the EEPROM
                    ultimate_nvm.delta = timerDelta;
                    //Save the timestamp
                    ultimate_nvm.timestamp = timerTx;
                    //save counter
                    ultimate_nvm.sync = ultimate_data.sync;
                    //save re-sync counter
                    ultimate_nvm.resync = ultimate_data.resync;
                    //Write back to NVM
                    write();

                    //Exit Learn mode
                    ultimate_mode = ULTIMATE_NORMAL_MODE;
                    ultimate_secure_learn_state = ULTIMATE_SECURE_LEARN_SEED;
                    validated_packet.result_code = LEARN_SUCCESS;
                    break; // case ULTIMATE_SECURE_LEARN_NORMAL:
                default:
                    break;
            }


        default:
            break;
    }

    return (uint8_t) validated_packet.result_code;
}

void keeloq_ultimate_processEvents(event_t event) {

    // This is the first state in which the ultimate KeeLoq initialises itself
    if (event == STARTUP_EVENT) {
        keeloq_ultimate_init();

#ifdef ULTIMATE_RESYNCH_RECEICER_ON_RTCC
        // if you do re-sync on RTCC
        //ultimate_resync_receiver_timer();
#endif

#ifdef ULTIMATE_RESYNCH_RECEICER_ON_TRANSMITTER
        // if you do re-sync on a learned encoder then you do a syncronous by setting the following:
        ultimate_mode = ULTIMATE_RESYNC;
#endif        
    }

    if (event == DELETE_ALL_EVENT) {
        ultimate_erase_all();
        ultimate_mode = ULTIMATE_NORMAL_MODE;
    }
    if (event == LEARN_EVENT)
        ultimate_mode = ULTIMATE_LEARN;
    if (event == SECURE_LEARN_EVENT)
        ultimate_mode = ULTIMATE_SECURE_LEARN;
    if ((event == LEARN_TIMOUT_EVENT) || (event == RESET_EVENT))
        ultimate_mode = ULTIMATE_NORMAL_MODE;


}

void keeloq_ultimate_decrypt_packet(ultimate_encoder_t* data, rawdata_packet_t* inputData) {

    uint8_t i;
    uint8_t isSeed;
    // First check what type of key generation it supports
    isSeed = 0;
    for (i = 0; i < 16; i++) {
        if (data->seed[i] != 0x00)
            isSeed = 1; // signal seed generated
    }

    if (isSeed == 0) //Is this a normal learned packet or a secure learned packet?
        // Compute the decryption key using serial number
        UltimateNormalKeyGen(&data->serialnumber);
    else {
        // Compute the decryption key using seed
        UltimateSeedKeyGen(data->seed);
    }

    // Decrypt the hopping code portion
    UltimateDecrypt((uint8_t*) & inputData->tmpBuffer[4]);
}

void ultimate_resync_receiver_timer() {
#ifdef ULTIMATE_RESYNCH_RECEICER_ON_RTCC
    // Get back-up timer data from the RTCC
    // not implemented yet
    ultimate_mode = ULTIMATE_NORMAL_MODE;
#endif
}

void keeloq_ultimate_init(void) {
    sync_time_init();
}

void ultimate_erase_all() {
    fopen(KEELOQ_ULTIMATE_FILE2);
    erase();
    fopen(KEELOQ_ULTIMATE_FILE1);
    erase();
    fopen(KEELOQ_ULTIMATE_FILE3);
    erase();
}

/*void transfer_to_record(ultimate_nvm_t ultimate_nvm) {
    currentRecord[0] = (uint8_t) (ultimate_nvm.serial & 0xff);
    currentRecord[1] = (uint8_t) ((ultimate_nvm.serial >> 8) & 0xff);
    currentRecord[2] = (uint8_t) ((ultimate_nvm.serial >> 16) & 0xff);
    currentRecord[3] = (uint8_t) ((ultimate_nvm.serial >> 24) & 0xff);
    currentRecord[4] = (uint8_t) ((ultimate_nvm.delta >> 0) & 0xff);
    currentRecord[5] = (uint8_t) ((ultimate_nvm.delta >> 8) & 0xff);
    currentRecord[6] = (uint8_t) ((ultimate_nvm.delta >> 16) & 0xff);
    currentRecord[7] = (uint8_t) ((ultimate_nvm.delta >> 24) & 0xff);
    currentRecord[8] = (uint8_t) ((ultimate_nvm.timestamp >> 0) & 0xff);
    currentRecord[9] = (uint8_t) ((ultimate_nvm.timestamp >> 8) & 0xff);
    currentRecord[10] = (uint8_t) ((ultimate_nvm.timestamp >> 16) & 0xff);
    currentRecord[11] = (uint8_t) ((ultimate_nvm.timestamp >> 24) & 0xff);
    currentRecord[12] = (uint8_t) ((ultimate_nvm.sync >> 0) & 0xff);
    currentRecord[13] = (uint8_t) ((ultimate_nvm.sync >> 8) & 0xff);
    currentRecord[14] = (uint8_t) ((ultimate_nvm.sync >> 16) & 0xff);
    currentRecord[15] = (uint8_t) (ultimate_nvm.resync & 0xff);
    currentRecord[16] = (uint8_t) ((ultimate_nvm.resync >> 8) & 0xff);
    currentRecord[17] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 0) & 0xff);
    currentRecord[18] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 8) & 0xff);
    currentRecord[19] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 16) & 0xff);
    currentRecord[20] = (uint8_t) ((ultimate_nvm.sync_timestamp >> 24) & 0xff);
}

void extract_decrypted_data(rawdata_packet_t tmpPacket) {
    ultimate_data.resync = ((uint16_t) tmpPacket.tmpBuffer[5] << 8) | ((uint16_t) tmpPacket.tmpBuffer[4]);
    ultimate_data.btn_timer = ((uint16_t) tmpPacket.tmpBuffer[7] << 8) | ((uint16_t) tmpPacket.tmpBuffer[6]);
    ultimate_data.ls_timer = ((int32_t) tmpPacket.tmpBuffer[11] << 24) | ((int32_t) tmpPacket.tmpBuffer[10] << 16) | ((int32_t) tmpPacket.tmpBuffer[9] << 8) | ((int32_t) tmpPacket.tmpBuffer[8]);
    ultimate_data.fcode = (tmpPacket.tmpBuffer[12]);
    ultimate_data.batt_level = (tmpPacket.tmpBuffer[13]);
    ultimate_data.sync = ((uint24_t) tmpPacket.tmpBuffer[16] << 16) | ((uint24_t) tmpPacket.tmpBuffer[15] << 8) | ((uint24_t) tmpPacket.tmpBuffer[14]);
    ultimate_data.delta_time = ((uint24_t) tmpPacket.tmpBuffer[19] << 16) | ((uint24_t) tmpPacket.tmpBuffer[18] << 8) | ((uint24_t) tmpPacket.tmpBuffer[17]);
}*/

/*void transfer_from_record(ultimate_nvm_t ultimate_nvm) {
    
}*/