/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */
#include <stdbool.h>
#include <stdint.h>
#include "encoder_mediator.h"
#include "rxa_raw.h"
//#include "keeloq_classic.h"
//#include "keeloq_advanced.h"
#include "keeloq_ultimate.h"
//#include "hardware.h"



validated_packet_t validated_packet;
extern uint16_t packet;
//validated_p2p_packet_t validated_p2p_packet; // For P2P requests

bool encoder_mediator(void) {

    rawdata_packet_t inData;

    inData.BitCount = rxi_read(inData.tmpBuffer, sizeof (inData.tmpBuffer));

    if (inData.BitCount > 0) {
/*#ifdef __DEBUG
        T0IE = 0;
#endif*/
        /*if (Decode_Classic_KeeLoq(&inData) != NOT_VALID_PACKET) {
            //Do something here.
        } *///else if (Decode_Advanced_KeeLoq(&inData) != NOT_VALID_PACKET) {
            //Do something here.
        //else 
        /*if (Decode_Classic_KeeLoq(&inData) != NOT_VALID_PACKET) {
        	//packet++;
        	validated_packet1.type = KEELOQ_CLASSIC;
            //Do something here.
        }
        else*/ if (Decode_Ultimate_KeeLoq(&inData) != NOT_VALID_PACKET){
        	//packet++;
        	validated_packet.type = KEELOQ_ULTIMATE;
        }
        else {
            validated_packet.type = INVALID_ENCODER;
        }
        //LED_TOG;
    } else {
        validated_packet.result_code = NO_DATA;
        return false;
    }

    return true;
}

void enmed_processEvent(event_t event) {

    //keeloq_classic_processEvents(event);
    //keeloq_advanced_processEvents(event);
    keeloq_ultimate_processEvents(event);

}

/**
 * Process P2P data requests from any attached listeners on the I2C
 */
/*void enmed_processP2P(UART_P2P_COMMANDS cmd) {
    validated_packet_t* data;

    // Important! Clear p2p data

    (validated_packet_t*) keeloq_classic_processP2P(cmd); // First query classic
    if (validated_p2p_packet.result_code == NO_DATA) {
        // Query Advanced
        (validated_packet_t*) keeloq_advanced_processP2P(cmd);
    } // NO 'else if' here
    if (validated_p2p_packet.result_code == NO_DATA) {
        // Query Ultimate
        (validated_packet_t*) keeloq_ultimate_processP2P(cmd);
    }

    // validated_p2p_packet.data is already filled with correct data
    //memcpy(validated_p2p_packet.data, &data->data, 8);
}
*/



