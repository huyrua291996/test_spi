/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */

/*#ifdef __XC8
#include <xc.h>
#else
#include <pic.h>
#endif*/
#include <xc.h>
//#include <stdint.h>
#include "tick.h"
#include "i2c_rtc.h"
#include "keeloq_ultimate.h"
#include "sync_timer.h"
extern ultimate_state_t ultimate_mode;
//uint32_t sync_timer;
extern volatile uint32_t syncTickCounter;

uint8_t daysOfMonth[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
uint8_t y, mo, d, h, mi, s;
void getTimer() {
    /*uint32_t out;
    // snapshot timer Rx
    di();
    out = sync_timer | (TMR1H >> 5);
    if(TMR1IF)  //check to see if we've overflowed between now and the previous instruction (very small chance)
        out = (sync_timer + 8) | (TMR1H >> 5); //add overflow ammount
    ei();*/
    
    y = Get_Year();
    mo = Get_Month();
    d = Get_Day();
    h = Get_Hour();
    mi = Get_Minute();
    s = Get_Second();
    Get_Count_Number();
    //return (uint32_t)(syncTickCounter);
}

void Get_Count_Number(void) {
    counter_number = d - 1;
    //uint32_t days = day - 1;
    if (mo > 1) {
        for (uint8_t i = 1; i < mo; i++) {
            counter_number += daysOfMonth[i-1];
        }
    }
    if ((mo > 2)&&((y % 4) == 0)) {
        counter_number++;
    }
    if ((mo > 2)&&((y % 100) == 0)&&((y % 400) != 0)) {
        counter_number--;
    }
    counter_number += (y - 1) * 365 + ((y + 3) >> 2) - 1;
    counter_number = counter_number * 24 * 60 * 60 + (int32_t)h * 60 * 60 + (int32_t)mi * 60 + (int32_t)s;
    counter_number = (int32_t)(counter_number *4);
}

void setTimer(uint32_t  in) {
    uint8_t year, month, day, hour, minute, second, leap, daysPerMonth;
    uint16_t days;
    in = in >> 2;
    second = in % 60;
    in /= 60;
    minute = in % 60;
    in /= 60;
    hour = in % 24;
    days = in / 24;
    for (year = 1;;year++)
    {
        leap = (year%4==0)?1:0;
        if (days < (365+leap)) break;
        days -= (365+leap);
    }
    for (month = 1;;month++)
    {
        daysPerMonth = daysOfMonth[month];
        if ((leap == 1)&&(month == 2)) daysPerMonth++;
        if (days < daysPerMonth) break;
        days -= daysPerMonth;
    }
    day = days;
    Set_Time(year, month, day, hour, minute, second);
    //syncTickCounter = (uint32_t) (in);
}
void sync_time_init()
{
    // TMR1 uses 32 kHz external crystal
    /*T1GCON = 0x00; // Ensure that T1 Gate is disabled
    T1CONbits.nT1SYNC = 1; // Do not sync with system clock
    T1CONbits.TMR1CS = 0b10; // external clock as source
    T1CONbits.T1OSCEN = 1; // enable oscillator driver
    T1CONbits.T1CKPS = 0; // 1:1 prescaler

    TMR1 = 0;
    TMR1H = 0;
    PIE1bits.TMR1IE = 1;
    PIR1bits.TMR1IF = 0;
    T1CONbits.TMR1ON = 1;*/

    //ultimate_mode = ULTIMATE_RESYNC;
    //syncTickCounter = 0;

}

