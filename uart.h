/* 
 * File:   uart.h
 * Author: huyru
 *
 * Created on January 8, 2020, 9:26 AM
 */

#ifndef UART_H
#define	UART_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>

    void UART_Init(void);
    void UART_SendChar(uint8_t ch);
    void UART_SendString(uint8_t* str);
    //void UART_ISR(void);

#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */

