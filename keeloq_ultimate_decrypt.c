/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "keeloq_ultimate_decrypt.h"
#include "nvm_mediator.h"
#include "aes.h"

static uint8_t key1[16];
static uint8_t MACkey[16];

void UltimateLoadMACKey(void);

void* getUltimateManufCode(void){
    UltimateLoadManufCode();
    return key1;
}

void setUltimateManufCode(const uint8_t* data){
    memcpy(key1, data, 16);
}

void* getUltimateAuthKey(void){
    UltimateLoadMACKey();
    return MACkey;
}

void setUltimateAuthKey(const uint8_t* data){
    memcpy(MACkey, data, 16);
}

void UltimateLoadMACKey(void)
{
    MACkey[0] = 0x13;
    MACkey[1] = 0x08;
    MACkey[2] = 0x19;
    MACkey[3] = 0x86;
    MACkey[4] = 0x05;
    MACkey[5] = 0x02;
    MACkey[6] = 0x19;
    MACkey[7] = 0x89;
    MACkey[8] = 0x18;
    MACkey[9] = 0x05;
    MACkey[10] = 0x20;
    MACkey[11] = 0x14;
    MACkey[12] = 0x29;
    MACkey[13] = 0x03;
    MACkey[14] = 0x20;
    MACkey[15] = 0x16;
}


void UltimateLoadManufCode(void) {
    //Load the manufacturing key
    key1[0] = 0x00;
    key1[1] = 0x09;
    key1[2] = 0x00;
    key1[3] = 0x09;
    key1[4] = 0x00;
    key1[5] = 0x00;
    key1[6] = 0x00;
    key1[7] = 0x05;
    key1[8]  = 0x01;
    key1[9]  = 0x00;
    key1[10] = 0x00;
    key1[11] = 0x08;
    key1[12] = 0x02;
    key1[13] = 0x00;
    key1[14] = 0x01;
    key1[15] = 0x07;
}

//----------------------------------------------------------------------
//
// Key Generation routine
//
// Normal Learn algorithm
//
// INPUT:  Serial Number (Buffer[4..7])
//         Manufacturer code
// OUTPUT: key1[0..15] computed decryption key
//
//----------------------------------------------------------------------
void UltimateNormalKeyGen(uint32_t* serial)
{
    uint8_t tmpblock[4];
    uint8_t block[16];
    uint8_t i;

    memcpy(tmpblock, serial, 4);

    UltimateLoadManufCode();

    block[0] = 0xA5; // Padding for first part
    block[1] = 0xA5;
    block[2] = 0xA5;
    block[3] = 0xA5;
    block[4] = 0x5A; // Padding second part
    block[5] = 0x5A;
    block[6] = 0x5A;
    block[7] = 0x5A;
    block[8] = tmpblock[3]; // Serial Number. Needs to be reversed
    block[9] = tmpblock[2];
    block[10] = tmpblock[1];
    block[11] = tmpblock[0];
    block[12] = 0x00; // Padding for last part
    block[13] = 0x00;
    block[14] = 0x00;
    block[15] = 0x00;

    AESCalcDecodeKey(key1); // Perform key generation for Decode
    AESDecode(block, key1); // Decode

    for (i = 0; i < 16; i++) {
        key1[i] = block[i]; // Result is the encryption key to be used
    }
}


void UltimateDecrypt(uint8_t* packet) {
    uint8_t block[16];

    memcpy(block, packet, 16);  // Copy data into block

    // Pre-calculate decode key
    AESCalcDecodeKey(key1);    // Perform key generation for Decode
    AESDecode(block, key1);   // Decode
    memcpy(packet, block, 16);  // Copy block back into packet

}

/** Calculate the device decryption key from the device seed. */
void UltimateSeedKeyGen(uint8_t* seed)
{
    /*uint8_t block[16];
    uint8_t i;
    // use the seed to get the decryption key
    memcpy(block, seed, 16);  // Copy data into block
    UltimateLoadManufCode();
    // Pre-calculate decode key
    AESCalcDecodeKey(key1);    // Perform key generation for Decode
    AESDecode(block,key1);

    for (i = 0; i < 16; i++) {
        key1[i] = block[i]; // Result is the encryption key to be used
    }*/
}

bool keeloq_ultimate_check_mac(ultimate_encoder_t* data) {

    uint8_t block2[16];

    //Stage 1: Calculate using serial number padded with zeros
    UltimateLoadMACKey();
    memset(block2, 0, 16);
    memcpy(block2, &data->serialnumber, 4);
    AESEncode(block2, MACkey); // Encode

    //Stage 2: Calculate using stage 1 result XOR'd with encrypted data
    UltimateLoadMACKey();
    uint8_t i;
    for( i = 0; i < 16; i++){
        block2[i] = data->raw_data[i] ^ block2[i];
    }
    AESEncode(block2, MACkey); // Encode

    if (memcmp(&data->mac, block2, UltimateMACLength) == 0)
        return true;
    else
        return false;
}








