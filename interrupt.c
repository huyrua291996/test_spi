#include <xc.h>
#include "sys_timer.h"
#include "adc.h"
#include "i2c.h"
void interrupt myISR(void)
{
    if ((PIR0bits.TMR0IF == 1)&&(PIE0bits.TMR0IE == 1))
        TIMER_ISR();
    if ((PIR1bits.ADIF == 1)&&(PIE1bits.ADIE == 1))
        ADC_ISR();
    if ((PIR3bits.SSP2IF == 1)&&(PIE3bits.SSP2IE == 1))
        I2C1_ISR();
}
