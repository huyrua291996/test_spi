/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */

/* 
 * File:   encoder_types.h
 *
 * Created on April 11, 2013, 2:55 PM
 */

#ifndef ENCODER_TYPES_H
#define	ENCODER_TYPES_H

#include <stdint.h>
#include <stdbool.h>

//#include "commands.h" // For P2P

/**
 * \note This needs to be the size of the biggest encoder struct
 * In this case it is ultimate_encoder_t at 60
 */
#define     ENM_MAX_SIZE            60


//Max packet size is KeeLoq Ultimate at 24
#define     ENM_MAX_PACKET_SIZE     24

typedef enum  {
    KEELOQ_CLASSIC,
    KEELOQ_ADVANCED,
    KEELOQ_ULTIMATE,
    INVALID_ENCODER,
}  encoder_type_t;

typedef enum  {
    NO_DATA,    ///< No Transmission Received
    VALID_PACKET, /**< Valid Packet Received*/
    NOT_VALID_PACKET,
    NOT_LEARNED,
    SEED_RECEIVED,
    NOT_SEED_PACKET,
    LEARN_SUCCESS,
    NO_SPACE,
    DECRYPT_FAIL,
    RESYNC_REQ,
    HOP_CHECK_FAIL,
    NVM_WRITE_ERROR,
    MAC_ERROR,
    TIMER_OUT_OF_SYNC,
    ENCODER_TIMER_RESYNC,
    TX_LOW_VOLTAGE,
    CNT_OUT_OF_SYNC
    //Add more Errors here
}  result_code_t;

typedef enum  {
    LEARN_EVENT,
    SECURE_LEARN_EVENT,
    LEARN_TIMOUT_EVENT,
    RESET_EVENT,
    RELEASE_EVENT,
    STARTUP_EVENT,
    ULTIMATE_RESYNC_EVENT,
    DELETE_ALL_EVENT
    //Add more Events here
}  event_t;

typedef struct  {
    encoder_type_t type;
    result_code_t result_code;
    uint8_t data[ENM_MAX_SIZE];
}  validated_packet_t;


extern validated_packet_t validated_packet;


typedef struct
{
    uint8_t BitCount;
    uint8_t tmpBuffer[ENM_MAX_PACKET_SIZE];
}  rawdata_packet_t;

typedef struct  {
    uint32_t mac;
    uint16_t resync;
    uint16_t btn_timer;
    int32_t ls_timer;
    uint8_t  fcode;
    uint8_t  batt_level;
    uint24_t sync;
    uint24_t delta_time;
    uint32_t serialnumber;
    uint8_t  raw_data[16];
    uint8_t  seed[16];
    int32_t rx_timestamp;
} ultimate_encoder_t;

typedef struct  {
    uint32_t serial;
    int32_t delta;
    int32_t timestamp;
    uint24_t sync;
    uint16_t resync;
    int32_t sync_timestamp; 
    uint8_t  seed[16];       
} ultimate_nvm_t;




#endif	/* ENCODER_TYPES_H */

