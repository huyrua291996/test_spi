#include "adc.h"
//#include "math.h"

static measure_state_t measure_state = VOLTAGE_MEASURE;
measure_result_t measure_result;
uint16_t voltage_read = 0, current_read = 0;

void ADC_Init(void)
{
    ADCON0 = 0x4D; //RC3, ADC on
    ADCON1 = 0xA0; //right format, Fosc / 32 = 1 MHz,Vref = Vdd
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    PIE1bits.ADIE = 1;
    PIR1bits.ADIF = 0;
}

void ADC_ISR(void)
{
    static uint8_t voltage_cnt = 0, current_cnt = 0;
    static uint32_t voltage_square = 0, current_square = 0;    
    PIR1bits.ADIF = 0;
    switch (measure_state)
    {
        case VOLTAGE_MEASURE:
            voltage_read = ADRESH;
            voltage_read = (voltage_read << 8) | ADRESL;
            voltage_square += voltage_read * voltage_read;
            voltage_cnt++;
            if (voltage_cnt >= 199)
            {
                voltage_cnt = 0;
                measure_result.voltage = (float)voltage_square / 200;
                voltage_square = 0;
            }
            measure_state = CURRENT_MEASURE;
            ADCON0 = 0x49;  //RC2
            break;
        case CURRENT_MEASURE:
            current_read = ADRESH;
            current_read = (current_read << 8) | ADRESL;
            current_square += current_read * current_read;
            current_cnt++;
            if (current_cnt >= 199)
            {
                current_cnt = 0;
                measure_result.current = (float) current_square / 200;
                current_square = 0;
            }
            measure_state = VOLTAGE_MEASURE;
            ADCON0 = 0x4D;
            break;
        default: break;
    }
}
