#include <xc.h>
#include "sys_timer.h"
#include "tick.h"
#include "rxa_raw.h"
#include <stdint.h>
#include <stdbool.h>

extern uint8_t time_flag/*, time_test*/;
bool readTime = false; 

void Timer_Init(void)
{
    T0CON0bits.T0EN = 1;
    T0CON0bits.T016BIT = 1; //16bit
    T0CON0bits.T0OUTPS = 0;
    T0CON1bits.T0CS0 = 0;
    T0CON1bits.T0CS1 = 1;
    T0CON1bits.T0CS2 = 0; // Fosc / 4
    T0ASYNC = 1; //not sync
    TMR0H = 0xFC; //120us
    TMR0L = 0x3F;
    PIR0bits.TMR0IF = 0;
    PIE0bits.TMR0IE = 1;
}

void TIMER_ISR(void)
{
    static uint8_t time_stage = 0, adc_time = 0;
    static uint16_t timer_cnt = 0;
    PIR0bits.TMR0IF = 0;
    TMR0H = 0xFC;
    TMR0L = 0x3F;
    rxi_tasks();
    time_stage++;  
    adc_time++;
    if (adc_time >= 4)
    {
        ADCON0bits.GOnDONE = 1;
        adc_time = 0;
    }
    if (time_stage >= 8)
    {
        SYSTICK_TASK();
        timer_cnt++;
        if (timer_cnt >= 999)
        {
            timer_cnt = 0;
            readTime = true;
        }
        /*adc_time++;
        if (adc_time >= 20)
        {
            adc_time = 0;
            ADCON0bits.GOnDONE = 1;
        }*/        
        time_stage = 0;
        time_flag = 1;
    }
}

