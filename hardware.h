/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */

#ifndef HARDWARE_H
#define	HARDWARE_H

#ifdef __XC8
#include <xc.h>
#else
#include <pic.h>
#endif


//#define _XTAL_FREQ 16000000
//SPI To EEPROM
#define CS                      LATCbits.LATC5
#define TRIS_CS                 TRISCbits.TRISC5
#define MISO                    PORTCbits.RC4       
#define MOSI                    LATCbits.LATC6      
#define SCK                     LATCbits.LATC7      

#define TRIS_MISO               TRISCbits.TRISC4
#define TRIS_MOSI               TRISCbits.TRISC6
#define TRIS_SCK                TRISCbits.TRISC7

//I2C RTCC
#define SDA                     LATBbits.LATB1      
#define SCL                     LATBbits.LATB2      

#define SDA_IN                  PORTBbits.RB1
#define SCL_IN                  PORTBbits.RB2

#define TRIS_SDA                TRISBbits.TRISB1
#define TRIS_SCL                TRISBbits.TRISB2

#define SDA_WPU                 WPUBbits.WPUB1
#define SCL_WPU                 WPUBbits.WPUB2

//GPIO from PICtail
#define PICtail_RAW_DATA                PORTDbits.RD5
#define TRIS_PICtail_RAW_DATA           TRISDbits.TRISD5

//LEDS
#define LED             LATEbits.LATE2
#define TRIS_LED        TRISEbits.TRISE2
#define LED_ON          1
#define LED_OFF         0
// button
#define LEARN_BUTTON            PORTAbits.RA3 
#define TRIS_LEARN_BUTTON       TRISAbits.TRISA3 
#define ANSEL_LEARN_BUTTON      ANSELAbits.ANSA3
#define ODCON_LEARN_BUTTON      ODCONAbits.ODCA3

#define RESET_BUTTON            PORTAbits.RA2 
#define TRIS_RESET_BUTTON       TRISAbits.TRISA2 
#define ANSEL_RESET_BUTTON      ANSELAbits.ANSA2
#define ODCON_RESET_BUTTON      ODCONAbits.ODCA2

#define SW1_BUTTON              PORTAbits.RA0 
#define TRIS_SW1_BUTTON         TRISAbits.TRISA0 
#define ANSEL_SW1_BUTTON        ANSELAbits.ANSA0
#define ODCON_SW1_BUTTON        ODCONAbits.ODCA0

#define SW2_BUTTON              PORTAbits.RA1 
#define TRIS_SW2_BUTTON         TRISAbits.TRISA1 
#define ANSEL_SW2_BUTTON        ANSELAbits.ANSA1
#define ODCON_SW2_BUTTON        ODCONAbits.ODCA1

//input
#define INPUT1              PORTEbits.RE0 
#define TRIS_INPUT1         TRISEbits.TRISE0 
#define ANSEL_INPUT1        ANSELEbits.ANSE0

#define INPUT2              PORTEbits.RE1 
#define TRIS_INPUT2         TRISEbits.TRISE1 
#define ANSEL_INPUT2        ANSELEbits.ANSE1

#define INPUT3              PORTAbits.RA7 
#define TRIS_INPUT3         TRISAbits.TRISA7 
#define ANSEL_INPUT3        ANSELAbits.ANSA7

#define INPUT4              PORTBbits.RB0 
#define TRIS_INPUT4         TRISBbits.TRISB0 
#define ANSEL_INPUT4        ANSELBbits.ANSB0

#define INPUT5              PORTBbits.RB4
#define TRIS_INPUT5         TRISBbits.TRISB4 
#define ANSEL_INPUT5        ANSELBbits.ANSB4

#define INPUT6              PORTBbits.RB5 
#define TRIS_INPUT6         TRISBbits.TRISB5 
#define ANSEL_INPUT6        ANSELBbits.ANSB5

//V, I sense
#define TRIS_I_SENSE         TRISCbits.TRISC2 
#define ANSEL_I_SENSE        ANSELCbits.ANSC2

#define TRIS_V_SENSE         TRISCbits.TRISC3 
#define ANSEL_V_SENSE        ANSELCbits.ANSC3

//Relay
#define RELAY1             LATAbits.LATA6
#define TRIS_RELAY1        TRISAbits.TRISA6
#define RELAY_ON          1
#define RELAY_OFF         0

#define RELAY2             LATAbits.LATA4
#define TRIS_RELAY2        TRISAbits.TRISA4

//buzzer
#define BUZZER             LATDbits.LATD4
#define TRIS_BUZZER        TRISDbits.TRISD4
#define BUZZER_ON          1
#define BUZZER_OFF         0

#define GPIO_CHECK_RESET 	300
#define GPIO_CHECK_HIGH		595
#define GPIO_CHECK_LOW		5

#endif
