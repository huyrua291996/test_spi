/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */

/* 
 * File:   nvm_mediator.h
 *
 * Created on April 8, 2013, 1:25 PM
 */

#ifndef NVM_MEDIATOR_H
#define	NVM_MEDIATOR_H

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "ftypes.h"
//#include "nvm_storage.h"
#include "encoder_types.h" //including this breaks dependency model, but we've included it to automate size of nvm data

#define NVM_KEELOQ_ULTIMATE_COUNT        1

typedef enum
{
    KEELOQ_ULTIMATE_FILE1 = 0,
    KEELOQ_ULTIMATE_FILE2 = 1,
    KEELOQ_ULTIMATE_FILE3 = 2
} nvm_file_type_t;

// 24 bytes file structure, this has to match file types above exactly as index
extern FILE      systemFiles[3];
extern uint16_t currentposition;
extern uint8_t   currentRecord[sizeof(ultimate_nvm_t)]; // Represents the largest record size supported
nvm_file_type_t   currentFile;

#define fseek(position)  currentposition = position;
#define fopen(type)  currentFile = type;

bool find(void * id);

void*     read(void);
uint8_t   write(void);
void      erase(void);
uint8_t   add_serial(void* record);


#endif	/* NVM_MEDIATOR_H */

