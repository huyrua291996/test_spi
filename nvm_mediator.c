/*
 * � 2014 Microchip Technology Inc. and its subsidiaries.  You may use
 * this software and any derivatives exclusively with Microchip
 * products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS
 * INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY OTHER
 * PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE
 * OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN
 * IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S
 * TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
 * WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
 * OF THESE TERMS.
 */

#include "nvm_mediator.h"
//#include "nvm_storage.h" @todo move this for access to fread inside keeloq_classic/advanced/ultimate
#include "ftypes.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include "eeprom.h"


//       File Space Usage
//     ======================
//     =  KEELOQ_ULTIMATE1  =
//     =                    =
//     =      64 bytes      =
//     ======================
//     =  KEELOQ_ULTIMATE2  =
//     =                    =
//     =      64 bytes      =
//     ======================
//     =  KEELOQ_ULTIMATE3  =
//     =                    =
//     =      64 bytes      =
//     ======================

// Needed for P2P
//#define NVM_KEELOQ_CLASSIC_COUNT         2
//#define NVM_KEELOQ_ADVANCED_COUNT        2
//#define NVM_KEELOQ_ULTIMATE_COUNT        2

#define NVM_KEELOQ_ULTIMATE1_START         0
#define NVM_KEELOQ_ULTIMATE1_END          (NVM_KEELOQ_ULTIMATE1_START + (sizeof(ultimate_nvm_t) * NVM_KEELOQ_ULTIMATE_COUNT) -17)

#define NVM_KEELOQ_ULTIMATE2_START         64
#define NVM_KEELOQ_ULTIMATE2_END          (NVM_KEELOQ_ULTIMATE2_START + (sizeof(ultimate_nvm_t) * NVM_KEELOQ_ULTIMATE_COUNT) -17)

#define NVM_KEELOQ_ULTIMATE3_START         128
#define NVM_KEELOQ_ULTIMATE3_END          (NVM_KEELOQ_ULTIMATE3_START + (sizeof(ultimate_nvm_t) * NVM_KEELOQ_ULTIMATE_COUNT) -17)

// As optimization the file types enum "nvm_file_type_t" closely mathces this by index
//FILE systemFiles[3] = {
//                        {  0,   59,   21,   1},
//                        {  64,  123,  21,   1},
//                        {  128, 197, 21,   1}
//                      };
FILE systemFiles[3] = {
    { NVM_KEELOQ_ULTIMATE1_START, NVM_KEELOQ_ULTIMATE1_END, 21, NVM_KEELOQ_ULTIMATE_COUNT},
    { NVM_KEELOQ_ULTIMATE2_START, NVM_KEELOQ_ULTIMATE2_END, 21, NVM_KEELOQ_ULTIMATE_COUNT},
    { NVM_KEELOQ_ULTIMATE3_START, NVM_KEELOQ_ULTIMATE3_END, 21, NVM_KEELOQ_ULTIMATE_COUNT}
};

nvm_file_type_t currentFile;
uint16_t currentposition;
uint8_t currentRecord[21];

/**
 *
 * Attempts to find a matching record given the serial number
 *
 * @param void*id  pointer to a data packet of bytes that contain the serial to search against
 * @pre fopen()
 * @return boolean indicating if found matching serial
 */
bool find(void * id) {
    uint16_t dataLocation = systemFiles[currentFile].start; // Fetch the start position for this type of record
    uint8_t recordSize = systemFiles[currentFile].recordsize;
    uint16_t dataEnd = systemFiles[currentFile].end;

    fseek(dataLocation);
    
    for (; dataLocation < dataEnd; dataLocation += recordSize) // Loop through the records
    {
        fread(&currentRecord[0], recordSize, &currentposition); // Assumes currentRecord is pointed to the start of the serial number AND  size of 32-bits
        // If this is a match        
        if (memcmp(id, currentRecord, 4) == 0) {
            fseek(dataLocation); /// Move the file pointer back to the current record, fread moved it to the next
            return true;
        }
    }

    return false;
}

/**
 * @pre @see find() to set the currentRecord position
 */
void* read(void) {
    return currentRecord;
}

uint8_t write(void) {
    return fwrite(&currentRecord[0], systemFiles[currentFile].recordsize, &currentposition);
}

uint8_t add_serial(void* record) {
#ifdef __XC8
    uint8_t addr[4]; //In XC8, Do not use initializers for local variables
    addr[0] = 0xFF; //because it creates two copies.
    addr[1] = 0xFF;
    addr[2] = 0xFF;
    addr[3] = 0xFF;
#else
    uint8_t addr[] = {0xFF, 0xFF, 0xFF, 0xFF};
#endif

    /*if (find(addr) == false) //return false if no space available. Empty space is determined by 4 bytes of 0xFF
        return 0;*/

    return fwrite(record, 4, &currentposition);
}


void erase(void) {
    uint16_t dataLocation = systemFiles[currentFile].start; // Fetch the start position for this type of record
    uint8_t recordSize = systemFiles[currentFile].recordsize;
    uint16_t dataEnd = systemFiles[currentFile].end;

    fseek(dataLocation);
    for (; dataLocation < dataEnd; dataLocation += recordSize) {
        // fill currentRecord with 0xFF
        memset(currentRecord, 0xFF, recordSize);
        fwrite(&currentRecord[0], recordSize, &currentposition);
    }
}


