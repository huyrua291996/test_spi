#include "uart.h"

void UART_Init(void)
{
    TX1STAbits.TXEN = 1;
    TX1STAbits.SYNC = 0;
    TX1STAbits.TX9 = 0;
    TX1STAbits.BRGH = 0;
    RC1STAbits.SPEN = 1;
    SP1BRGLbits.SP1BRGL = 51; //32M/9600/64-1
}

void UART_SendChar(uint8_t ch)
{
    while (TX1STAbits.TRMT == 0);
    TX1REGbits.TX1REG = ch;
}

void UART_SendString(uint8_t* str)
{
    while (*str)
    {
        UART_SendChar(*str++);
    }
}

/*void UART_ISR(void)
{
    
}*/