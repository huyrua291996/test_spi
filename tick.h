/*
 * tick.h
 *
 *  Created on: Feb 21, 2019
 *      Author: huyru
 */

#ifndef TICK_H_
#define TICK_H_

#include <xc.h>
#include <stdint.h>
//#include <pic.h>
//#include "type.h"
#define SYSTICK_TIMEOUT		0
#define SYSTICK_TIME_MS(x)	x
#define SYSTICK_TIME_SEC(x)	(x*1000)
#define SYSTICK_SECOND 1000

volatile uint32_t syncTickCounter = 0;

typedef struct {
	uint32_t start_time; 		
	uint32_t timeout;
	uint32_t crc;
} Timeout_Type;

uint32_t CheckTimeout(Timeout_Type *t);
uint32_t SysTick_Get(void);
uint32_t SYSTICK_GETSEC(void);
void SYSTICK_TASK(void);
void TimerDelayms(uint32_t time);
//void delay1us(void);
//void delayus(uint16_t time);
//void delayms(uint32_t time);
void InitTimeout(Timeout_Type *t,uint32_t timeout);


#endif /* TICK_H_ */
