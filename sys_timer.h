/* 
 * File:   sys_timer.h
 * Author: huyru
 *
 * Created on December 25, 2019, 11:00 AM
 */

#ifndef SYS_TIMER_H
#define	SYS_TIMER_H
#include <xc.h>
#ifdef	__cplusplus
extern "C" {
#endif
    
    void Timer_Init(void);
    void TIMER_ISR(void);

#ifdef	__cplusplus
}
#endif

#endif	/* SYS_TIMER_H */

