/* 
 * File:   eeprom_i2c.h
 * Author: A16686
 *
 * Created on October 26, 2015, 2:18 PM
 */

#ifndef EEPROM_I2C_H
#define	EEPROM_I2C_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>
    
#define MAX_RETRY         100
#define SLAVE_ADDRESS     0x6F 
#define PAGE_LIMIT         16  // Change as stated on EEPROM device datasheet

int I2C_ByteWrite(uint8_t *dataAddress, uint8_t dataByte, uint8_t addlen);
uint8_t I2C_ByteRead(uint8_t *dataAddress, uint8_t dataByte,uint8_t addlen);
int I2C_BufferWrite(uint8_t *dataAddress, uint8_t *dataBuffer,  uint8_t addlen, uint8_t buflen);
void I2C_BufferRead(uint8_t *dataAddress, uint8_t *dataBuffer,  uint8_t addlen, uint8_t buflen);
uint8_t I2C_ByteRead1(uint8_t dataAddress);
void I2C_ByteWrite1(uint8_t dataAddress, uint8_t dataByte);
uint8_t Get_Year(void);
uint8_t Get_Month(void);
uint8_t Get_Day(void);
uint8_t Get_Hour(void);
uint8_t Get_Minute(void);
uint8_t Get_Second(void);

void Set_Time(uint8_t year, uint8_t month, uint8_t day,
                uint8_t hour, uint8_t minute, uint8_t second);
void MCP_Start(void);

#ifdef	__cplusplus
}
#endif

#endif	/* EEPROM_I2C_H */

