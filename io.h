/* 
 * File:   io.h
 * Author: huyru
 *
 * Created on December 25, 2019, 3:04 PM
 */

#ifndef IO_H
#define	IO_H

#include <xc.h>
#include "hardware.h"
#ifdef	__cplusplus
extern "C" {
#endif

    void IO_Init(void);
    void IO_Check(void);


#ifdef	__cplusplus
}
#endif

#endif	/* IO_H */

