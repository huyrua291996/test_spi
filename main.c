/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.77
        Device            :  PIC16F15376
        Driver Version    :  2.00
 */

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
 */

#include "mcc_generated_files/mcc.h"
#include "eeprom.h"
#include "encoder_mediator.h"
#include "sys_timer.h"
#include "io.h"
#include "task.h"
#include "i2c.h"
#include "i2c_rtc.h"
#include "uart.h"
#include "tick.h"
#include "sync_timer.h"
#include "adc.h"
//#include <stdio.h>
#include "nvm_mediator.h"
#include "keeloq_ultimate.h"
#include "xc.h"
/*
                         Main application
 */

uint16_t led_time, buzzer_time;
uint16_t led_time_tick, buzzer_time_tick;
uint8_t led_repeat, buzzer_repeat;
uint8_t time_flag = 0;
//uint8_t info[15];
extern bool readTime;
extern int32_t counter_number;

void main(void) {
    // initialize the device
    SYSTEM_Initialize();
    ADC_Init();
    Timer_Init();
    I2C1_Initialize();
    IO_Init();
    UART_Init();
    CS_LAT = 1;
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    MCP_Start();
    enmed_processEvent(STARTUP_EVENT);   
    while (1) {
        // Add your application code 
        //time_test = 0;
        if (encoder_mediator() == true) {
            Keeloq_Task();
        }
        //UART_SendChar(time_test);
        if (time_flag == 1) {
            IO_Check();
            RF_Task();
            Control_Task();
            LED_Task();
            Buzzer_Task();
            //getTimer();
            if (readTime == true) {
                getTimer();
                //sprintf(info, "now: %ld\n", counter_number);
                UART_SendChar((uint8_t)((counter_number >> 24)&0xff));
                UART_SendChar((uint8_t)((counter_number >> 16)&0xff));
                UART_SendChar((uint8_t)((counter_number >> 8)&0xff));
                UART_SendChar((uint8_t)((counter_number)&0xff));
                UART_SendChar('\n');
                readTime = false;
            }
            time_flag = 0;

        }
        /*#asm
                wfi
        #endasm*/
    }
}
/**
 End of File
 */